import React, { useContext, useEffect, useState } from "react";
import config from "../customize/config";
import { Row, Col, Card, Button, Checkbox, Modal, Input, Form } from "antd";
import {
  CheckoutForm,
  initialCheckoutForm,
} from "../components/tc-domain/checkout/Checkout";
import {
  CloseOutlined,
  LoadingOutlined,
  RightOutlined,
} from "@ant-design/icons";
import { useRouter } from "next/router";
import { TravelCloudClient } from "../core/travelcloud";
import { getContent } from "../core/cms-compute";
import LayoutContext from "../components/layouts/LayoutContext";
import { Order } from "../components/tc-domain/order/Order";

const Checkout = ({ termContent }) => {
  const router = useRouter();
  const { cart, order } = useContext(LayoutContext);
  const terms = getContent(termContent).tag("terms").getValue();
  console.log(cart);
  const [form] = Form.useForm();

  const orderResult = order?.result;

  const checkout: any = initialCheckoutForm(orderResult, false);

  if (checkout != null) {
    checkout.travelers = []; // comment this line to checkout with travelers details
    checkout.phone_country = { value: "65" }; // default phone country to SG
  }

  const [submittingOrder, setSubmittingOrder] = useState(false);
  const [orderLoading, setOrderLoading] = useState(true);
  const [error, setError] = useState(null);
  const [errorRef, setErrorRef] = useState(null);
  const [userClickedSubmit, setUserClickedSubmit] = useState(null);
  const [showTerms, setShowTerms] = useState(false);
  const [acceptTerms, setAcceptTerms] = useState(false);
  const [checkoutForm, setCheckoutForm] = useState(checkout || null);
  const [remarks, setRemarks] = useState("");

  useEffect(() => {
    router.prefetch("/payment"); // prefetch payment page
  }, []);

  useEffect(() => {
    if (orderLoading === true && orderResult != null) {
      setTimeout(() => window.scrollTo(0, 0), 100);
      setOrderLoading(false);
      setCheckoutForm(checkoutForm);
    }
  }, []);

  const onSubmit = async (e) => {
    e.preventDefault();
    setUserClickedSubmit(true);
    try {
      const values = await form.validateFields();
      if (acceptTerms) {
        setSubmittingOrder(true);
        // cannot checkout yet
        // this.client.post error happen for travelcentral
        const result = await cart.checkout({
          ...values,
          user_message: remarks,
        });
        if (result.result == null) {
          setError(true);
        } else {
          if (result.result.order_status === "Quotation") {
            router.push("/payment?ref=" + result.result.ref);
          } else {
            setError(true);
            setErrorRef(result.result.ref);
          }
        }
      }
    } catch (error) {
      setError(true);
    }
  };

  const onChange = (e) => {
    setAcceptTerms(e.target.checked);
    setUserClickedSubmit(true);
  };

  if (error === true) {
    return (
      <div className="wrap pad-y">
        <h1>We have receive your booking</h1>
        <p>
          Our travel consultants will be contacting you to follow up with your
          booking.
        </p>
        {errorRef != null && <p>Order reference: {errorRef}</p>}
      </div>
    );
  }

  if (orderResult?.loading) {
    return (
      <div className="wrap pad-y">
        <Card loading={true} style={{ border: 0 }} />
      </div>
    );
  }

  if (
    orderResult == null ||
    orderResult.products == null ||
    orderResult.products.length === 0
  ) {
    return (
      <div className="wrap pad-y">
        <h1>Your cart is empty</h1>
      </div>
    );
  }

  const gotPayment =
    orderResult?.products.filter((i) => i.product_type.includes("hotel"))
      .length > 0;

  return (
    <article id="checkout" className="grey">
      <section id="checkout-form" className="wrap pad-y">
        <Row justify="space-between" gutter={{ lg: 24 }}>
          <Col xs={24} sm={24} md={24} lg={9}>
            <section id="order-details">
              <h2>Order Details</h2>
              <Order
                order={orderResult}
                showSection={{ products: true }}
                cart={cart}
              />
            </section>
          </Col>
          <Col xs={24} sm={24} md={24} lg={15}>
            <section id="details">
              <h2>Contact Details</h2>
              <CheckoutForm form={form} />
              <h2>Additional Information</h2>
              <div className="center-block remarks">
                <Input.TextArea
                  rows={4}
                  value={remarks}
                  onChange={(e) => setRemarks(e.target.value)}
                  placeholder={
                    !!orderResult.products &&
                    orderResult.products.filter((a) =>
                      a.product_type.includes("nontour_product")
                    ).length === 0
                      ? `Please indicate bed type for each room.`
                      : orderResult.products.filter(
                          (a) =>
                            a.product_type.includes("nontour_product") ||
                            a.product_type.includes("tour_package")
                        ).length > 0
                      ? `For Visa: Please state date of entry & departure.`
                      : ``
                  }
                />
              </div>

              <div
                id="extra-fields"
                className={userClickedSubmit && !acceptTerms ? "error" : ""}
              >
                <Checkbox
                  checked={acceptTerms}
                  onChange={onChange}
                  className="m-r-15"
                />
                I have read, understood and accepted the{" "}
                <a onClick={() => setShowTerms(true)}>terms and conditions.</a>
              </div>

              <Modal
                visible={showTerms}
                wrapClassName="terms-and-condition"
                onCancel={() => setShowTerms(false)}
                closable={false}
                title={
                  <Row>
                    <Col span={20}>TERMS AND CONDITIONS</Col>
                    <Col span={4} style={{ textAlign: "right" }}>
                      <a onClick={() => setShowTerms(false)}>
                        <CloseOutlined />
                      </a>
                    </Col>
                  </Row>
                }
                footer={false}
              >
                {terms && (
                  <div
                    className="innerHTML"
                    dangerouslySetInnerHTML={{
                      __html: terms.content || "",
                    }}
                  />
                )}
              </Modal>
              <Button
                size="large"
                type="primary"
                disabled={submittingOrder}
                onClick={(e) => onSubmit(e)}
              >
                {gotPayment ? "Proceed to payment" : "Submit"}
                &nbsp;
                {submittingOrder ? <LoadingOutlined /> : <RightOutlined />}
              </Button>
            </section>
          </Col>
        </Row>
      </section>
    </article>
  );
};

Checkout.getInitialProps = async (context) => {
  const client = new TravelCloudClient(config.tcUser);
  const query = context.query;
  const termContent = await client.getDocument("terms", config.domain);
  return { query, termContent };
};

export default Checkout;
