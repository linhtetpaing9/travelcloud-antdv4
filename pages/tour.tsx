import { Form } from "antd";
import Head from "next/head";
import React, { useContext } from "react";
import LayoutContext from "../components/layouts/LayoutContext";
import TourTemplate from "../components/tc-domain/tour/TourTemplate";
import { TravelCloudClient } from "../core/travelcloud";
import config from "../customize/config";

function Tour({ query, tour }: any) {
  console.log(tour);
  const { cart } = useContext(LayoutContext);
  const [form] = Form.useForm();

  return (
    <div>
      <Head>
        <title>Travelcloud antd | v4 | Tour </title>
        <meta name="description" content="travelcloud-antd-v4/flights" />
      </Head>
      <TourTemplate tour={tour} cart={cart} form={form} />
    </div>
  );
}

Tour.getInitialProps = async (context: any) => {
  const client = new TravelCloudClient(config.tcUser);
  const tour = await client.tour(context.query);
  const query = context.query;
  return { tour, query };
};

export default Tour;
