import App from 'next/app'
import '../styles/styles.scss'

import type { AppProps } from 'next/app'
import { useEffect } from 'react'
import Link from "next/link";
import SiderLayout from '../components/layouts/SiderLayout'
import { Button, Result } from 'antd'
import React from 'react';
import { setupUserback, TravelCloudClient } from '../core/travelcloud';
import config from '../customize/config';

interface ObjectLiteral {
  [key: string]: any;
}

type ServerSideProps = {
  notFound: boolean,
  content: ObjectLiteral,
  priceRules: any
} & AppProps

function MyApp({
  Component,
  pageProps,
  priceRules,
  notFound,
}: ServerSideProps) {

  useEffect(() => {
    setupUserback();
  }, [])

  const backToHome = (
    <Link href="/">
      <a>
        <div className="flex-button">
          <Button type="primary" size="large">
            Back Home
        </Button>
        </div>
      </a>
    </Link>
  );

  return (
    <SiderLayout priceRules={priceRules}>
      {notFound ? (
        <Result
          status={500}
          title="Error"
          subTitle="Sorry, something went wrong."
          extra={backToHome}
        />
      ) : pageProps["statusCode"] ? (
        <Result
          status={pageProps["statusCode"]}
          title={`Error: ${pageProps["statusCode"]}`}
          subTitle="Sorry, something went wrong."
          extra={backToHome}
        />
      ) : (
        <Component {...pageProps} />
      )}
    </SiderLayout>
  )
}

MyApp.getInitialProps = async (appContext: any) => {
  const appProps = await App.getInitialProps(appContext);
  try {
    const client = new TravelCloudClient(config.tcUser);
    const priceRules = await client.priceRules({ is_public: "1" });
    return { ...appProps, priceRules };
  } catch (error) {
    return {
      notFound: true
    }
  }
}

export default MyApp
