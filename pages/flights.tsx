import Head from "next/head";
import React, { useContext } from "react";
import LayoutContext from "../components/layouts/LayoutContext";
import FlightTemplate from "../components/tc-domain/flights/FlightTemplate";


function Flights({ query }: any) {
  const { cart, client } = useContext(LayoutContext)
  return (
    <div>
      <Head>
        <title>Travelcloud antd | v4 | Flights </title>
        <meta name="description" content="travelcloud-antd-v4/flights" />
      </Head>
      <FlightTemplate query={query} client={client} cart={cart}/>
    </div>
  );
}

Flights.getInitialProps = async (context: any) => {
  const { query } = context;
  return { query };
};

export default Flights;
