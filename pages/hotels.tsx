import Head from "next/head";
import React, { useContext } from "react";
import LayoutContext from "../components/layouts/LayoutContext";
import HotelTemplate from "../components/tc-domain/hotels/HotelTemplate";

function Hotels({ query }: any) {
  const { cart, client } = useContext(LayoutContext);
  return (
    <div>
      <Head>
        <title>Travelcloud antd | v4 | Hotels </title>
        <meta name="description" content="travelcloud-antd-v4/hotels" />
      </Head>
      <HotelTemplate query={query} client={client} cart={cart} />
    </div>
  );
}

Hotels.getInitialProps = async (context: any) => {
  const { query } = context;
  return { query };
};

export default Hotels;
