import Head from "next/head";
import React from "react";

function Home({ query, content }: any) {
  return (
    <div>
      <Head>
        <title>Travelcloud antd | v4 </title>
        <meta name="description" content="travelcloud-antd-v4" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <h1>Travelcloud here</h1>
    </div>
  );
}

Home.getInitialProps = async (context: any) => {
  const { query } = context;
  return { query };
};

export default Home;
