export type PriceRule = {
  id: string;
  name: string;
  description: string;
  is_public: string;
  use_count_quantity: string;
  adjustment_name: string;
  adjustment_sequence: string;
  adjustment_price_type: string;
  adjustment_per_unit: string;
  adjustment_percentage: string;
  adjustment_percentage_price_attribute: string;
  adjustment_product_id_final_per_unit_csv: string;
  condition_checkout_code: string;
  condition_checkout_code_hash: string;
  condition_mutex_code: string;
  condition_maximum_use_count_quantity: string;
  condition_from_datetime: string;
  condition_to_datetime: string;
  condition_product_source: string;
  condition_product_type: string;
  condition_minimum_product_quantity: string;
  condition_product_in_any_categories_enabled: string;
  condition_customer_in_any_categories: Array<{
    id: string;
    category_type: string;
    name: string;
    parent: string;
    description: string;
    image_url: string;
    license_url: string;
    create_date: string;
  }>;
  condition_product_in_any_categories: Array<{
    id: string;
    category_type: string;
    name: string;
    parent: string;
    description: string;
    image_url: string;
    license_url: string;
    create_date: string;
  }>;
};
export type PriceRules = Array<{
  id: string;
  name: string;
  description: string;
  is_public: string;
  use_count_quantity: string;
  adjustment_name: string;
  adjustment_sequence: string;
  adjustment_price_type: string;
  adjustment_per_unit: string;
  adjustment_percentage: string;
  adjustment_percentage_price_attribute: string;
  adjustment_product_id_final_per_unit_csv: string;
  condition_checkout_code: string;
  condition_checkout_code_hash: string;
  condition_mutex_code: string;
  condition_maximum_use_count_quantity: string;
  condition_from_datetime: string;
  condition_to_datetime: string;
  condition_product_source: string;
  condition_product_type: string;
  condition_minimum_product_quantity: string;
  condition_product_in_any_categories_enabled: string;
  condition_customer_in_any_categories: Array<{
    id: string;
    category_type: string;
    name: string;
    parent: string;
    description: string;
    image_url: string;
    license_url: string;
    create_date: string;
  }>;
  condition_product_in_any_categories: Array<{
    id: string;
    category_type: string;
    name: string;
    parent: string;
    description: string;
    image_url: string;
    license_url: string;
    create_date: string;
  }>;
}>;
export type ApiLog = {
  id: string;
  action: string;
  api: string;
  api_ref: string;
  table: string;
  table_id: string;
  has_error: string;
  error: string;
  date_added: string;
};
export type ApiLogs = Array<{
  id: string;
  action: string;
  api: string;
  api_ref: string;
  table: string;
  table_id: string;
  has_error: string;
  error: string;
  date_added: string;
}>;
export type Document = {
  id: string;
  name: string;
  code: string;
  content: string;
  photo_url: string;
  photo_license_url: string;
  sort_order: string;
  attributes: object;
  date: string;
  categories: Array<{
    id: string;
    category_type: string;
    name: string;
    parent: string;
    description: string;
    image_url: string;
    license_url: string;
    create_date: string;
  }>;
  photos: Array<{
    id: string;
    title: string;
    desc: string;
    url: string;
    license_url: string;
    sequence: string;
  }>;
};
export type Documents = Array<{
  id: string;
  name: string;
  code: string;
  content: string;
  photo_url: string;
  photo_license_url: string;
  sort_order: string;
  attributes: object;
  date: string;
  categories: Array<{
    id: string;
    category_type: string;
    name: string;
    parent: string;
    description: string;
    image_url: string;
    license_url: string;
    create_date: string;
  }>;
  photos: Array<{
    id: string;
    title: string;
    desc: string;
    url: string;
    license_url: string;
    sequence: string;
  }>;
}>;
export type Admin = {
  id: string;
  first_name: string;
  last_name: string;
  email: string;
  create_date: string;
  deleted: string;
  roles: {
    "customer-manage": boolean;
    "price-rule": boolean;
    "tour-product": boolean;
    "generic-product": boolean;
    "visa-product": boolean;
    "order-find": boolean;
    "order-manual-quote": boolean;
    "order-payment-add": boolean;
    "order-quotation-expiry": boolean;
    "order-status": boolean;
    "order-product-status": boolean;
    "order-report": boolean;
    site: boolean;
    "admin-super": boolean;
  };
  restrict_referral_site: string;
  notify_new_invoice: string;
  notify_new_conversation: string;
  failed_login_count: string;
  whitelist_ipv4_start: string;
  whitelist_ipv4_end: string;
  reduce_2fa: string;
  last_30_days_tokens: Array<{
    id: string;
    roles: string;
    restrict_referral_site: string;
    ip: string;
    ip_country: string;
    user_agent: string;
    issued: string;
    expiry: string;
    whitelist_ipv4_start: string;
    whitelist_ipv4_end: string;
  }>;
};
export type Admins = Array<{
  id: string;
  first_name: string;
  last_name: string;
  email: string;
  create_date: string;
  deleted: string;
  roles: {
    "customer-manage": boolean;
    "price-rule": boolean;
    "tour-product": boolean;
    "generic-product": boolean;
    "visa-product": boolean;
    "order-find": boolean;
    "order-manual-quote": boolean;
    "order-payment-add": boolean;
    "order-quotation-expiry": boolean;
    "order-status": boolean;
    "order-product-status": boolean;
    "order-report": boolean;
    site: boolean;
    "admin-super": boolean;
  };
  restrict_referral_site: string;
  notify_new_invoice: string;
  notify_new_conversation: string;
  failed_login_count: string;
  whitelist_ipv4_start: string;
  whitelist_ipv4_end: string;
  reduce_2fa: string;
  last_30_days_tokens: Array<{
    id: string;
    roles: string;
    restrict_referral_site: string;
    ip: string;
    ip_country: string;
    user_agent: string;
    issued: string;
    expiry: string;
    whitelist_ipv4_start: string;
    whitelist_ipv4_end: string;
  }>;
}>;
export type Category = {
  id: string;
  category_type: string;
  name: string;
  parent: string;
  description: string;
  image_url: string;
  license_url: string;
  create_date: string;
};
export type Categories = Array<{
  id: string;
  category_type: string;
  name: string;
  parent: string;
  description: string;
  image_url: string;
  license_url: string;
  create_date: string;
}>;
export type Conversation = {
  id: string;
  referral_site: string;
  status: string;
  subject: string;
  created: string;
  last_updated: string;
  messages: Array<{
    id: string;
    admin_id: string;
    verified: string;
    customer_name: string;
    customer_email: string;
    html: string;
    unstructured: object;
    unstructured_shape: string;
    date: string;
    admin: {
      id: string;
      first_name: string;
      last_name: string;
      email: string;
      create_date: string;
      deleted: string;
      roles: {
        "customer-manage": boolean;
        "price-rule": boolean;
        "tour-product": boolean;
        "generic-product": boolean;
        "visa-product": boolean;
        "order-find": boolean;
        "order-manual-quote": boolean;
        "order-payment-add": boolean;
        "order-quotation-expiry": boolean;
        "order-status": boolean;
        "order-product-status": boolean;
        "order-report": boolean;
        site: boolean;
        "admin-super": boolean;
      };
      restrict_referral_site: string;
      notify_new_invoice: string;
      notify_new_conversation: string;
      failed_login_count: string;
      whitelist_ipv4_start: string;
      whitelist_ipv4_end: string;
      reduce_2fa: string;
    };
  }>;
};
export type Conversations = Array<{
  id: string;
  referral_site: string;
  status: string;
  subject: string;
  created: string;
  last_updated: string;
  messages: Array<{
    id: string;
    admin_id: string;
    verified: string;
    customer_name: string;
    customer_email: string;
    html: string;
    unstructured: object;
    unstructured_shape: string;
    date: string;
    admin: {
      id: string;
      first_name: string;
      last_name: string;
      email: string;
      create_date: string;
      deleted: string;
      roles: {
        "customer-manage": boolean;
        "price-rule": boolean;
        "tour-product": boolean;
        "generic-product": boolean;
        "visa-product": boolean;
        "order-find": boolean;
        "order-manual-quote": boolean;
        "order-payment-add": boolean;
        "order-quotation-expiry": boolean;
        "order-status": boolean;
        "order-product-status": boolean;
        "order-report": boolean;
        site: boolean;
        "admin-super": boolean;
      };
      restrict_referral_site: string;
      notify_new_invoice: string;
      notify_new_conversation: string;
      failed_login_count: string;
      whitelist_ipv4_start: string;
      whitelist_ipv4_end: string;
      reduce_2fa: string;
    };
  }>;
}>;
export type Generic = {
  id: string;
  name: string;
  description: string;
  photo_url: string;
  photo_license_url: string;
  sort_order: string;
  banner_photo_id: string;
  attributes: object;
  options: Array<{
    id: string;
    code: string;
    name: string;
    stock: string;
    stock_type: string;
    cost: string;
    price: string;
    files: Array<{
      id: string;
      key: string;
      name: string;
      mime_type: string;
      size: string;
      crc32: string;
      notification_id: string;
      added_date: string;
    }>;
  }>;
  categories: Array<{
    id: string;
    category_type: string;
    name: string;
    parent: string;
    description: string;
    image_url: string;
    license_url: string;
    create_date: string;
  }>;
  photos: Array<{
    id: string;
    title: string;
    desc: string;
    url: string;
    license_url: string;
    sequence: string;
  }>;
};
export type Generics = Array<{
  id: string;
  name: string;
  description: string;
  photo_url: string;
  photo_license_url: string;
  sort_order: string;
  banner_photo_id: string;
  attributes: object;
  options: Array<{
    id: string;
    code: string;
    name: string;
    stock: string;
    stock_type: string;
    cost: string;
    price: string;
    files: Array<{
      id: string;
      key: string;
      name: string;
      mime_type: string;
      size: string;
      crc32: string;
      notification_id: string;
      added_date: string;
    }>;
  }>;
  categories: Array<{
    id: string;
    category_type: string;
    name: string;
    parent: string;
    description: string;
    image_url: string;
    license_url: string;
    create_date: string;
  }>;
  photos: Array<{
    id: string;
    title: string;
    desc: string;
    url: string;
    license_url: string;
    sequence: string;
  }>;
}>;
export type Order = {
  id: string;
  ref: string;
  user_message: string;
  account_id: string;
  bill_uid: string;
  order_date: string;
  order_status: string;
  referral_site: string;
  payment_status: string;
  product_status: string;
  deposit_required: string;
  payment_required: string;
  payment_received: string;
  first_name: string;
  last_name: string;
  phone_country: string;
  phone: string;
  address: string;
  checkout_code: string;
  expiry: string;
  adult: string;
  child: string;
  infant: string;
  unstructured: object;
  customer: {
    id: string;
    name: string;
    phone: string;
    email: string;
    registered: string;
    email_subscription: string;
    email_verified: string;
    title: string;
    categories: Array<{
      id: string;
      category_type: string;
      name: string;
      parent: string;
      description: string;
      image_url: string;
      license_url: string;
      create_date: string;
    }>;
  };
  travelers: Array<{
    id: string;
    passport: string;
    passport_issue_country: string;
    passport_issue_date: string;
    expiry: string;
    title: string;
    first_name: string;
    last_name: string;
    country: string;
    residence_country: string;
    birth_date: string;
    type: string;
    loyalty_carrier: string;
    loyalty_number: string;
  }>;
  payments: Array<{
    id: string;
    source: string;
    source_id: string;
    amount: string;
    fee: string;
    description: string;
    date_added: string;
    details: string;
    status: string;
  }>;
  products: Array<{
    id: string;
    product_source: string;
    product_source_ref: string;
    product_source_ref2: string;
    product_source_pending_status: string;
    product_source_pending_timestamp: string;
    product_type: string;
    product_id: string;
    product_code: string;
    product_name: string;
    product_detail: object;
    product_category_ids: string;
    product_attributes: object;
    quantity: string;
    from_date: string;
    from_time: string;
    to_date: string;
    to_time: string;
    product_status: string;
    voucher: object;
    form: object;
    items: Array<{
      id: string;
      header: string;
      sub_header: string;
      name: string;
      price: string;
      deposit: string;
      price_type: string;
      quantity: string;
      price_rule_id: string;
    }>;
  }>;
};
export type Orders = Array<{
  id: string;
  ref: string;
  user_message: string;
  account_id: string;
  bill_uid: string;
  order_date: string;
  order_status: string;
  referral_site: string;
  payment_status: string;
  product_status: string;
  deposit_required: string;
  payment_required: string;
  payment_received: string;
  first_name: string;
  last_name: string;
  phone_country: string;
  phone: string;
  address: string;
  checkout_code: string;
  expiry: string;
  adult: string;
  child: string;
  infant: string;
  unstructured: object;
  customer: {
    id: string;
    name: string;
    phone: string;
    email: string;
    registered: string;
    email_subscription: string;
    email_verified: string;
    title: string;
  };
  travelers: Array<{
    id: string;
    passport: string;
    passport_issue_country: string;
    passport_issue_date: string;
    expiry: string;
    title: string;
    first_name: string;
    last_name: string;
    country: string;
    residence_country: string;
    birth_date: string;
    type: string;
    loyalty_carrier: string;
    loyalty_number: string;
  }>;
  payments: Array<{
    id: string;
    source: string;
    source_id: string;
    amount: string;
    fee: string;
    description: string;
    date_added: string;
    details: string;
    status: string;
  }>;
  products: Array<{
    id: string;
    product_source: string;
    product_source_ref: string;
    product_source_ref2: string;
    product_source_pending_status: string;
    product_source_pending_timestamp: string;
    product_type: string;
    product_id: string;
    product_code: string;
    product_name: string;
    product_detail: object;
    product_category_ids: string;
    product_attributes: object;
    quantity: string;
    from_date: string;
    from_time: string;
    to_date: string;
    to_time: string;
    product_status: string;
    voucher: object;
    form: object;
    items: Array<{
      id: string;
      header: string;
      sub_header: string;
      name: string;
      price: string;
      deposit: string;
      price_type: string;
      quantity: string;
      price_rule_id: string;
    }>;
  }>;
}>;
export type Tour = {
  id: string;
  name: string;
  short_desc: string;
  itinerary: string;
  country: string;
  extras: string;
  remarks: string;
  photo_url: string;
  photo_license_url: string;
  max_beds: string;
  room_config: string;
  max_adults: string;
  max_children_no_bed: string;
  max_infants: string;
  price_type: string;
  sort_order: string;
  attributes: object;
  categories: Array<{
    id: string;
    category_type: string;
    name: string;
    parent: string;
    description: string;
    image_url: string;
    license_url: string;
    create_date: string;
  }>;
  options: Array<{
    id: string;
    product_code: string;
    name: string;
    type: string;
    min_lead_time: string;
    on_demand_advance_booking: string;
    duration: string;
    SGL: string;
    TWN: string;
    TRP: string;
    QUAD: string;
    CWB: string;
    CNB: string;
    CHT: string;
    INF: string;
    fixed_deposit: string;
    percentage_deposit: string;
    rule: string;
    rule_parsed: string;
    departures: Array<{
      id: string;
      date: string;
      slots_taken: string;
      slots_total: string;
    }>;
  }>;
  photos: Array<{
    id: string;
    title: string;
    desc: string;
    url: string;
    license_url: string;
    sequence: string;
  }>;
  tour_addons: Array<{
    id: string;
    name: string;
    description: string;
    default_option: string;
    image_url: string;
    license_url: string;
    create_date: string;
    options: Array<{
      id: string;
      name: string;
      currency: string;
      SGL: string;
      TWN: string;
      TRP: string;
      QUAD: string;
      CWB: string;
      CNB: string;
      CHT: string;
      INF: string;
    }>;
  }>;
  generic_addons: Array<{
    id: string;
    name: string;
    description: string;
    image_url: string;
    license_url: string;
    create_date: string;
    options: Array<{
      id: string;
      name: string;
      currency: string;
      price: string;
    }>;
  }>;
};
export type Tours = Array<{
  id: string;
  name: string;
  short_desc: string;
  itinerary: string;
  country: string;
  extras: string;
  remarks: string;
  photo_url: string;
  photo_license_url: string;
  max_beds: string;
  room_config: string;
  max_adults: string;
  max_children_no_bed: string;
  max_infants: string;
  price_type: string;
  sort_order: string;
  attributes: object;
  categories: Array<{
    id: string;
    category_type: string;
    name: string;
    parent: string;
    description: string;
    image_url: string;
    license_url: string;
    create_date: string;
  }>;
  options: Array<{
    id: string;
    product_code: string;
    name: string;
    type: string;
    min_lead_time: string;
    on_demand_advance_booking: string;
    duration: string;
    SGL: string;
    TWN: string;
    TRP: string;
    QUAD: string;
    CWB: string;
    CNB: string;
    CHT: string;
    INF: string;
    fixed_deposit: string;
    percentage_deposit: string;
    rule: string;
    rule_parsed: string;
    _subset_departures: Array<{
      id: string;
      date: string;
      slots_taken: string;
      slots_total: string;
    }>;
  }>;
  photos: Array<{
    id: string;
    title: string;
    desc: string;
    url: string;
    license_url: string;
    sequence: string;
  }>;
  tour_addons: Array<{
    id: string;
    name: string;
    description: string;
    default_option: string;
    image_url: string;
    license_url: string;
    create_date: string;
    options: Array<{
      id: string;
      name: string;
      currency: string;
      SGL: string;
      TWN: string;
      TRP: string;
      QUAD: string;
      CWB: string;
      CNB: string;
      CHT: string;
      INF: string;
    }>;
  }>;
  generic_addons: Array<{
    id: string;
    name: string;
    description: string;
    image_url: string;
    license_url: string;
    create_date: string;
    options: Array<{
      id: string;
      name: string;
      currency: string;
      price: string;
    }>;
  }>;
}>;
export type Customer = {
  id: string;
  name: string;
  phone: string;
  email: string;
  registered: string;
  email_subscription: string;
  email_verified: string;
  title: string;
  orders: Array<{
    id: string;
    ref: string;
    user_message: string;
    bill_uid: string;
    order_date: string;
    order_status: string;
    referral_site: string;
    payment_status: string;
    product_status: string;
    deposit_required: string;
    payment_required: string;
    payment_received: string;
    first_name: string;
    last_name: string;
    phone_country: string;
    phone: string;
    address: string;
    checkout_code: string;
    expiry: string;
    adult: string;
    child: string;
    infant: string;
    unstructured: object;
    customer: {
      id: string;
      name: string;
      phone: string;
      email: string;
      registered: string;
      email_subscription: string;
      email_verified: string;
      title: string;
    };
    products: Array<{
      id: string;
      product_source: string;
      product_source_ref: string;
      product_source_ref2: string;
      product_source_pending_status: string;
      product_source_pending_timestamp: string;
      product_type: string;
      product_id: string;
      product_code: string;
      product_name: string;
      product_detail: object;
      product_category_ids: string;
      product_attributes: object;
      quantity: string;
      from_date: string;
      from_time: string;
      to_date: string;
      to_time: string;
      product_status: string;
      voucher: object;
      form: object;
    }>;
  }>;
  additional_info: Array<{
    id: string;
    key: string;
    value: string;
    timestamp: string;
  }>;
  categories: Array<{
    id: string;
    category_type: string;
    name: string;
    parent: string;
    description: string;
    image_url: string;
    license_url: string;
    create_date: string;
    customer_in_any_price_rules: Array<{
      id: string;
      name: string;
      description: string;
      is_public: string;
      use_count_quantity: string;
      adjustment_name: string;
      adjustment_sequence: string;
      adjustment_price_type: string;
      adjustment_per_unit: string;
      adjustment_percentage: string;
      adjustment_percentage_price_attribute: string;
      adjustment_product_id_final_per_unit_csv: string;
      condition_checkout_code: string;
      condition_checkout_code_hash: string;
      condition_mutex_code: string;
      condition_maximum_use_count_quantity: string;
      condition_from_datetime: string;
      condition_to_datetime: string;
      condition_product_source: string;
      condition_product_type: string;
      condition_minimum_product_quantity: string;
      condition_product_in_any_categories_enabled: string;
      condition_customer_in_any_categories: Array<{
        id: string;
        category_type: string;
        name: string;
        parent: string;
        description: string;
        image_url: string;
        license_url: string;
        create_date: string;
      }>;
      condition_product_in_any_categories: Array<{
        id: string;
        category_type: string;
        name: string;
        parent: string;
        description: string;
        image_url: string;
        license_url: string;
        create_date: string;
      }>;
    }>;
  }>;
  payments: Array<{
    id: string;
    order_id: string;
    amount: string;
    fee: string;
    description: string;
    date_added: string;
    details: string;
    status: string;
    order: {
      id: string;
      ref: string;
      user_message: string;
      account_id: string;
      bill_uid: string;
      order_date: string;
      order_status: string;
      referral_site: string;
      payment_status: string;
      product_status: string;
      deposit_required: string;
      payment_required: string;
      payment_received: string;
      first_name: string;
      last_name: string;
      phone_country: string;
      phone: string;
      address: string;
      checkout_code: string;
      expiry: string;
      adult: string;
      child: string;
      infant: string;
      unstructured: object;
    };
  }>;
};
export type Customers = Array<{
  id: string;
  name: string;
  phone: string;
  email: string;
  registered: string;
  email_subscription: string;
  email_verified: string;
  title: string;
  categories: Array<{
    id: string;
    category_type: string;
    name: string;
    parent: string;
    description: string;
    image_url: string;
    license_url: string;
    create_date: string;
  }>;
}>;
export type Config = {
  Company: {
    COMPANY_NAME: string;
    COMPANY_LOGO: string;
    COMPANY_ADDRESS: string;
    COMPANY_PHONE: string;
    PAYMENT_CURRENCY: string;
    SYSTEM_EMAIL: string;
    DEFAULT_QUOTATION_EXPIRY_MINIUTES: string;
    FLIGHT_QUOTATION_EXPIRY_MINIUTES: string;
    TOUR_QUOTATION_EXPIRY_MINIUTES: string;
    GENERIC_QUOTATION_EXPIRY_MINIUTES: string;
    FLIGHT_FIXED_MARKUP: string;
    FLIGHT_PERCENTAGE_MARKUP: string;
    FLIGHT_MARKUP_PRECISION: string;
    FLIGHT_AIRLINE_BLACKLIST: string;
    FLIGHT_ORIGIN_WHITELIST: string;
    CUSTOMER_COUNTRY_WHITELIST: string;
    FLIGHT_LEAD_TIME: string;
    HOTEL_LEAD_TIME: string;
  };
  "Email Templates": {
    COMPANY_INVOICE_SUBJECT: string;
    COMPANY_INVOICE_TEMPLATE: string;
    FLIGHT_VOUCHER_SUBJECT: string;
    FLIGHT_VOUCHER_TEMPLATE: string;
  };
  Wirecard: {
    WIRECARD_HPP_URL: string;
    WIRECARD_MID: string;
    WIRECARD_SECRET: string;
    WIRECARD_PSP_NAME: string;
    WIRECARD_FEE_OFFSET_FIXED: string;
    WIRECARD_FEE_OFFSET_PERCENTAGE: string;
  };
  Paypal: {
    PAYPAL_EMAIL: string;
    PAYPAL_HOST: string;
    PAYPAL_FEE_OFFSET_FIXED: string;
    PAYPAL_FEE_OFFSET_PERCENTAGE: string;
  };
  PayDollar: {
    PAYDOLLAR_URL: string;
    PAYDOLLAR_MERCHANT_ID: string;
    PAYDOLLAR_SECURE_HASH_SECRET: string;
    PAYDOLLAR_FEE_OFFSET_FIXED: string;
    PAYDOLLAR_FEE_OFFSET_PERCENTAGE: string;
  };
  WebConnect: {
    WEBCONNECT_EPR: string;
    WEBCONNECT_PASSCODE: string;
    WEBCONNECT_IPCC: string;
    WEBCONNECT_AGENCY_PHONE_LOCATION_CODE: string;
    WEBCONNECT_AGENCY_PHONE_NUMBER: string;
    WEBCONNECT_PNR_CREATED_HOST_COMMANDS: string;
    WEBCONNECT_PAYMENT_RECEIVED_HOST_COMMANDS: string;
    WEBCONNECT_URL: string;
    WEBCONNECT_ACCOUNT_CODES: string;
    WEBCONNECT_PAYMENT_RECEIVED_QUEUE: string;
  };
  Travelport: {
    TRAVELPORT_USERNAME: string;
    TRAVELPORT_PASSWORD: string;
    TRAVELPORT_BRANCH_CODE: string;
    TRAVELPORT_AGENCY_NAME: string;
    TRAVELPORT_AGENCY_PHONE: string;
    TRAVELPORT_URL: string;
    TRAVELPORT_PAYMENT_RECEIVED_QUEUE: string;
  };
  Reddot: {
    REDDOT_URL: string;
    REDDOT_MERCHANT_ID: string;
    REDDOT_KEY: string;
    REDDOT_SECRET_KEY: string;
    REDDOT_FEE_OFFSET_FIXED: string;
    REDDOT_FEE_OFFSET_PERCENTAGE: string;
  };
  "Admin Payment": {
    ADMIN_PAYMENT_LABEL_1: string;
    ADMIN_PAYMENT_CHOICES_1: string;
    ADMIN_PAYMENT_LABEL_2: string;
    ADMIN_PAYMENT_CHOICES_2: string;
    ADMIN_PAYMENT_LABEL_3: string;
    ADMIN_PAYMENT_CHOICES_3: string;
    ADMIN_PAYMENT_LABEL_4: string;
    ADMIN_PAYMENT_CHOICES_4: string;
    ADMIN_PAYMENT_LABEL_5: string;
    ADMIN_PAYMENT_CHOICES_5: string;
  };
  Bedsonline: {
    BEDSONLINE_USERNAME: string;
    BEDSONLINE_PASSWORD: string;
    BEDSONLINE_URL: string;
    BEDSONLINE_SHOW_OPAQUE_CONTRACT: string;
    BEDSONLINE_FIXED_MARKUP: string;
    BEDSONLINE_PERCENTAGE_MARKUP: string;
  };
  "SQ NDC": {
    SQNDC_WSAP: string;
    SQNDC_LSSORG: string;
    SQNDC_LSSUSER: string;
    SQNDC_PASSWORD: string;
    SQNDC_IATA: string;
    SQNDC_OID: string;
    SQNDC_DUTYCODE: string;
    SQNDC_URL: string;
  };
  TTC: {
    TTC_ENV: string;
    TTC_API_KEY: string;
  };
  GlobalTix: {
    GLOBALTIX_URL: string;
    GLOBALTIX_USERNAME: string;
    GLOBALTIX_PASSWORD: string;
    GLOBALTIX_FIXED_MARKUP: string;
    GLOBALTIX_PERCENTAGE_MARKUP: string;
    GLOBALTIX_TICKETTYPE_ID_PRICE_CSV: string;
  };
  PKFARE: {
    PKFARE_PARTNER_ID: string;
    PKFARE_PARTNER_KEY: string;
    PKFARE_BASE_URL: string;
  };
  "First Data": {
    FIRST_DATA_URL: string;
    FIRST_DATA_STORE_ID: string;
    FIRST_DATA_SECRET: string;
    FIRST_DATA_FEE_OFFSET_FIXED: string;
    FIRST_DATA_FEE_OFFSET_PERCENTAGE: string;
  };
  "ABA PayWay": {
    ABA_PAYWAY_API_URL: string;
    ABA_PAYWAY_API_KEY: string;
    ABA_PAYWAY_MERCHANT_ID: string;
    ABA_PAYWAY_CARDS_FEE_OFFSET_FIXED: string;
    ABA_PAYWAY_CARDS_FEE_OFFSET_PERCENTAGE: string;
    ABA_PAYWAY_ABAPAY_FEE_OFFSET_FIXED: string;
    ABA_PAYWAY_ABAPAY_FEE_OFFSET_PERCENTAGE: string;
  };
  Hotelbeds: {
    HOTELBEDS_HOSTNAME: string;
    HOTELBEDS_API_KEY: string;
    HOTELBEDS_SECRET: string;
    HOTELBEDS_FIXED_MARKUP: string;
    HOTELBEDS_PERCENTAGE_MARKUP: string;
    HOTELBEDS_VOUCHER_SUBJECT: string;
    HOTELBEDS_VOUCHER_TEMPLATE: string;
    HOTELBEDS_FAILED_SUBJECT: string;
    HOTELBEDS_FAILED_TEMPLATE: string;
    HOTELBEDS_QUOTATION_EXPIRY_MINIUTES: string;
  };
  "Pay 2C2P": {
    PAY_2C2P_MERCHANT_ID: string;
    PAY_2C2P_SECRET_KEY: string;
    PAY_2C2P_PAYMENT_URL: string;
    PAY_2C2P_FEE_OFFSET_FIXED: string;
    PAY_2C2P_FEE_OFFSET_PERCENTAGE: string;
  };
  Mailchimp: {
    MAILCHIMP_API_KEY: string;
  };
  Sendinblue: {
    SENDINBLUE_API_KEY: string;
  };
};
export type FlightSearch = Array<{
  od1: {
    id: string;
    segments: Array<{
      marketing_carrier: {
        code: string;
        name: string;
      };
      flight_number: string;
      cabin: string;
      connected: boolean;
      operating_carrier: {
        code: string;
        name: string;
      };
      flights: Array<{
        departure_airport: {
          code: string;
          name: string;
        };
        departure_terminal: string;
        departure_datetime: string;
        arrival_airport: {
          code: string;
          name: string;
        };
        arrival_terminal: string;
        arrival_datetime: string;
        equipment: {
          code: string;
          name: string;
        };
      }>;
      flights_info: {
        origin: {
          code: string;
          name: string;
        };
        origin_terminal: string;
        origin_datetime: string;
        destination: {
          code: string;
          name: string;
        };
        destination_terminal: string;
        destination_datetime: string;
        total_minutes: number;
        equipment: {
          code: string;
          name: string;
        };
        stops: Array<{
          arrival_datetime: string;
          departure_datetime: string;
          airport: {
            code: string;
            name: string;
          };
          layover_minutes: number;
        }>;
      };
    }>;
  };
  od2: {
    id: string;
    segments: Array<{
      marketing_carrier: {
        code: string;
        name: string;
      };
      flight_number: string;
      cabin: string;
      connected: boolean;
      operating_carrier: {
        code: string;
        name: string;
      };
      flights: Array<{
        departure_airport: {
          code: string;
          name: string;
        };
        departure_terminal: string;
        departure_datetime: string;
        arrival_airport: {
          code: string;
          name: string;
        };
        arrival_terminal: string;
        arrival_datetime: string;
        equipment: {
          code: string;
          name: string;
        };
      }>;
      flights_info: {
        origin: {
          code: string;
          name: string;
        };
        origin_terminal: string;
        origin_datetime: string;
        destination: {
          code: string;
          name: string;
        };
        destination_terminal: string;
        destination_datetime: string;
        total_minutes: number;
        equipment: {
          code: string;
          name: string;
        };
        stops: Array<{
          arrival_datetime: string;
          departure_datetime: string;
          airport: {
            code: string;
            name: string;
          };
          layover_minutes: number;
        }>;
      };
    }>;
  };
  pricings: Array<{
    id: string;
    source: string;
    cost: string;
    price: string;
    currency: string;
    plating_carrier: string;
    trip_type: string;
    fares: Array<{
      fare_basis: string;
      fare_rule_key: string;
      trip_type: string;
      carrier_code: string;
      baggage_pieces: number;
      baggage_weight_amount: string;
      baggage_weight_unit: string;
      brand: {
        id: string;
        name: string;
        description: string;
        image: string;
        services: Array<{
          included: boolean;
          name: string;
          description: string;
        }>;
      };
      segments: Array<{
        origin: {
          code: string;
          name: string;
        };
        destination: {
          code: string;
          name: string;
        };
        booking_code: string;
        booking_code_availability: number;
        cabin: string;
      }>;
    }>;
    ptc_breakdown_map: {
      [key: string]: {
        quantity: number;
        cost: string;
        price: string;
        base_fare: string;
        currency: string;
        refundable: boolean;
      };
    };
  }>;
}>;
export type FlightDetail = {
  od1: {
    id: string;
    segments: Array<{
      marketing_carrier: {
        code: string;
        name: string;
      };
      flight_number: string;
      cabin: string;
      connected: boolean;
      operating_carrier: {
        code: string;
        name: string;
      };
      flights: Array<{
        departure_airport: {
          code: string;
          name: string;
        };
        departure_terminal: string;
        departure_datetime: string;
        arrival_airport: {
          code: string;
          name: string;
        };
        arrival_terminal: string;
        arrival_datetime: string;
        equipment: {
          code: string;
          name: string;
        };
      }>;
      flights_info: {
        origin: {
          code: string;
          name: string;
        };
        origin_terminal: string;
        origin_datetime: string;
        destination: {
          code: string;
          name: string;
        };
        destination_terminal: string;
        destination_datetime: string;
        total_minutes: number;
        equipment: {
          code: string;
          name: string;
        };
        stops: Array<{
          arrival_datetime: string;
          departure_datetime: string;
          airport: {
            code: string;
            name: string;
          };
          layover_minutes: number;
        }>;
      };
    }>;
  };
  od2: {
    id: string;
    segments: Array<{
      marketing_carrier: {
        code: string;
        name: string;
      };
      flight_number: string;
      cabin: string;
      connected: boolean;
      operating_carrier: {
        code: string;
        name: string;
      };
      flights: Array<{
        departure_airport: {
          code: string;
          name: string;
        };
        departure_terminal: string;
        departure_datetime: string;
        arrival_airport: {
          code: string;
          name: string;
        };
        arrival_terminal: string;
        arrival_datetime: string;
        equipment: {
          code: string;
          name: string;
        };
      }>;
      flights_info: {
        origin: {
          code: string;
          name: string;
        };
        origin_terminal: string;
        origin_datetime: string;
        destination: {
          code: string;
          name: string;
        };
        destination_terminal: string;
        destination_datetime: string;
        total_minutes: number;
        equipment: {
          code: string;
          name: string;
        };
        stops: Array<{
          arrival_datetime: string;
          departure_datetime: string;
          airport: {
            code: string;
            name: string;
          };
          layover_minutes: number;
        }>;
      };
    }>;
  };
  pricings: Array<{
    id: string;
    source: string;
    cost: string;
    price: string;
    currency: string;
    plating_carrier: string;
    trip_type: string;
    fares: Array<{
      fare_basis: string;
      fare_rule_key: string;
      trip_type: string;
      carrier_code: string;
      baggage_pieces: number;
      baggage_weight_amount: string;
      baggage_weight_unit: string;
      brand: {
        id: string;
        name: string;
        description: string;
        image: string;
        services: Array<{
          included: boolean;
          name: string;
          description: string;
        }>;
      };
      segments: Array<{
        origin: {
          code: string;
          name: string;
        };
        destination: {
          code: string;
          name: string;
        };
        booking_code: string;
        booking_code_availability: number;
        cabin: string;
      }>;
    }>;
    ptc_breakdown_map: {
      [key: string]: {
        quantity: number;
        cost: string;
        price: string;
        base_fare: string;
        currency: string;
        refundable: boolean;
      };
    };
  }>;
};
