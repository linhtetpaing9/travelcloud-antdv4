import Big from "big.js";
import moment from "moment";
import rulesCompute from "./rules-compute";
import {
  Tour,
  Generic,
  FlightDetail,
  Order,
  PriceRules,
  Customer,
  PriceRule,
} from "./types";
import bcrypt from "bcryptjs";

export type Item = {
  [P in keyof Order["products"][0]["items"][0]]?: Order["products"][0]["items"][0][P];
};

export type Product = {
  [P in keyof Pick<
    Order["products"][0],
    Exclude<keyof Order["products"][0], "items">
  >]?: Order["products"][0][P];
} & {
  items: Item[];
};

type PriceRuleWithCount = PriceRule & { _order_use_count_quantity?: number };

export type ComputeResult = {
  product?: Product;
  error?: string;
  total?: string;
};

export type FlightFormValue = {
  od1_id: string;
  od2_id?: string;
  pricing_id: string;
  ptc_adt: number;
  ptc_cnn?: number;
  ptc_inf?: number;
};

export type TourFormValue = {
  tour_id?: string;
  rooms?: {
    adult: number;
    child_with_bed?: number;
    child_no_bed?: number;
    infant?: number;
  }[];
  option_id?: string;
  departure_date?: string;
  tour_addons?: {
    id: string;
    option_id: string;
  }[];
  generic_addons?: {
    id: string;
    options: {
      id: string;
      quantity: string | number;
    }[];
  }[];
};

export type GlobaltixFormValue = {
  id: number;
  quantity: number;
  fromResellerId: number | null;
  visitDate?: string;
  questionList?: {
    id: number;
    answer: string;
  }[];
};

export type LegacyHotelFormValue = {
  room_id: string;
  price_code: string;
  params: {
    hotelId: string;
    checkInDate: string;
    duration: number | string;
    adults: number | string;
    children: number | string;
  };
};

export type HotelbedsFormValue = {
  sourceMarket?: string;

  stay: {
    checkIn: string;
    checkOut: string;
    shiftDays: number | string;
  };

  occupancies: {
    rooms: number;
    adults: number;
    children: number;
    paxes: {
      type: string;
      age: number;
    }[];
  }[];

  hotels: {
    hotel: number[];
  };

  destinationCode?: string;
  rateKey?: string;
  hotelCode?: string;
};

// fixme: to be removed
export type HotelbedsQuery = HotelbedsFormValue & {
  rateKey: string;
  hotelCode?: string;
  destionationCode?: string;
};

export type HotelbedsStaticData = {
  code: number;
};

export const decodePriceCode = {
  SGL: "Adult (SGL)",
  TWN: "Adult (TWN)",
  TRP: "Adult (TRP)",
  QUAD: "Adult (QUAD)",
  CWB: "Child (with bed)",
  CNB: "Child (sharing bed)",
  CHT: "Child (half twin)",
  INF: "Infant",
};

export function computeTour(
  tour: Tour,
  tourFormValue: TourFormValue
): ComputeResult {
  const optionSelected = tour.options.find(
    (option) => option.id === tourFormValue.option_id
  );
  if (optionSelected == null) {
    return {
      error: "option_id " + tourFormValue.option_id + " not found",
    };
  }

  const departureSelected = optionSelected.departures.find(
    (departure) => departure.date === tourFormValue.departure_date
  );

  // mutate tourFormValue to ensure everything is number
  // there is no way to enforce types at runtime
  tourFormValue.rooms = tourFormValue.rooms.map((room) => ({
    adult: parseInt(String(room.adult || 0), 10),
    child_with_bed: parseInt(String(room.child_with_bed || 0), 10),
    child_no_bed: parseInt(String(room.child_no_bed || 0), 10),
    infant: parseInt(String(room.infant || 0), 10),
  }));

  const adult = tourFormValue.rooms.reduce(
    (acc, room) => (room.adult || 0) + acc,
    0
  );
  const child = tourFormValue.rooms.reduce(
    (acc, room) => (room.child_with_bed || 0) + (room.child_no_bed || 0) + acc,
    0
  );
  const infant = tourFormValue.rooms.reduce(
    (acc, room) => (room.infant || 0) + acc,
    0
  );
  const traveler = adult + child;
  const tmpDepartureDate = new Date(tourFormValue.departure_date);

  const notPricingByRoom =
    tour.price_type.indexOf("ALL") === -1 &&
    tour.price_type.indexOf("SGL") === -1;

  const counter = {
    SGL: 0,
    TWN: 0,
    TRP: 0,
    QUAD: 0,
    CWB: 0,
    CNB: 0,
    CHT: 0,
    INF: 0,
  };

  const context = {
    adult,
    child,
    infant,
    traveler,
    slots_taken:
      departureSelected == null
        ? []
        : Array(traveler)
            .fill(null)
            .map((_, idx) => departureSelected.slots_taken + idx),
    departure_date: [tourFormValue.departure_date],
    travel_dates: Array(parseInt(optionSelected.duration, 10))
      .fill(null)
      .map((_, idx) => {
        const isoDate = dateToIsoDate(tmpDepartureDate);
        tmpDepartureDate.setDate(tmpDepartureDate.getDate() + 1);
        return isoDate;
      }),
    coupon: "",
    days_before_departure: Math.ceil(
      (new Date(tourFormValue.departure_date).getTime() -
        new Date().getTime()) /
        24 /
        60 /
        60 /
        1000
    ),
    room: tourFormValue.rooms.length,
  };

  const rulesResult = rulesCompute(optionSelected.rule_parsed, context);

  const effectivePrice: any = Object.assign({}, optionSelected);
  const currentPrice = rulesResult.prices.pop();
  if (currentPrice != null) {
    effectivePrice.name += " (" + currentPrice.name + ")";
    for (const j in currentPrice.price) {
      if (currentPrice.price.hasOwnProperty(j)) {
        const k = currentPrice.price[j];
        effectivePrice[k.code] = k.price;
      }
    }
  }

  const pushLines = (
    adultPriceType: string,
    price: any,
    subHeader: string,
    room: any,
    acc: any[]
  ) => {
    counter[adultPriceType] += room.adult;
    acc.push({
      sub_header: subHeader,
      name: "Adult",
      price: price[adultPriceType],
      deposit: Big(price["fixed_deposit"])
        .plus(
          Big(price[adultPriceType]).times(price["percentage_deposit"]).div(100)
        )
        .toFixed(2)
        .toString(),
      price_type: adultPriceType,
      quantity: room.adult,
    });

    let childBed = room.child_with_bed || 0;
    if (adult === 1 && childBed > 0) {
      childBed--;
      counter.CHT += 1;
      acc.push({
        sub_header: subHeader,
        name: decodePriceCode.CHT,
        price: effectivePrice["CHT"],
        deposit: Big(effectivePrice["fixed_deposit"])
          .plus(
            Big(effectivePrice["CHT"])
              .times(effectivePrice["percentage_deposit"])
              .div(100)
          )
          .toFixed(2)
          .toString(),
        price_type: "CHT",
        quantity: 1,
      });
    }

    if (childBed > 0) {
      counter.CWB += childBed;
      acc.push({
        sub_header: subHeader,
        name: decodePriceCode.CWB,
        price: effectivePrice["CWB"],
        deposit: Big(effectivePrice["fixed_deposit"])
          .plus(
            Big(effectivePrice["CWB"])
              .times(effectivePrice["percentage_deposit"])
              .div(100)
          )
          .toFixed(2)
          .toString(),
        price_type: "CWB",
        quantity: childBed,
      });
    }

    if ((room.child_no_bed || 0) > 0) {
      counter.CNB += room.child_no_bed;
      acc.push({
        sub_header: subHeader,
        name: decodePriceCode.CNB,
        price: effectivePrice["CNB"],
        deposit: Big(effectivePrice["fixed_deposit"])
          .plus(
            Big(effectivePrice["CNB"])
              .times(effectivePrice["percentage_deposit"])
              .div(100)
          )
          .toFixed(2)
          .toString(),
        price_type: "CNB",
        quantity: room.child_no_bed,
      });
    }

    if ((room.infant || 0) > 0) {
      counter.INF += room.infant;
      acc.push({
        sub_header: subHeader,
        name: decodePriceCode.INF,
        price: effectivePrice["INF"],
        deposit: Big(effectivePrice["fixed_deposit"])
          .plus(
            Big(effectivePrice["INF"])
              .times(effectivePrice["percentage_deposit"])
              .div(100)
          )
          .toFixed(2)
          .toString(),
        price_type: "INF",
        quantity: room.infant,
      });
    }
  };

  const invoiceItems = tourFormValue.rooms.reduce((acc, room, index) => {
    const subHeader =
      tourFormValue.rooms.length > 1 ? "Room " + (index + 1) : "Base price";
    const beds = room.adult + (room.child_with_bed || 0);
    if (
      tour.price_type.indexOf("ALL") === -1 &&
      tour.price_type.indexOf("SGL") === -1 &&
      tour.price_type.indexOf("CWB") === -1
    ) {
      const totalTravellers =
        (room.adult || 0) +
        (room.child_no_bed || 0) +
        (room.child_with_bed || 0);
      if (totalTravellers > 0) {
        counter.TWN += totalTravellers;
        acc.push({
          sub_header: subHeader,
          name: "Traveller",
          price: effectivePrice["TWN"],
          deposit: Big(effectivePrice["fixed_deposit"])
            .plus(
              Big(effectivePrice["TWN"])
                .times(effectivePrice["percentage_deposit"])
                .div(100)
            )
            .toFixed(2)
            .toString(),
          price_type: "TWN",
          quantity: totalTravellers,
        });
      }

      return acc;
    }

    if (notPricingByRoom) {
      if ((room.adult || 0) > 0) {
        counter.TWN += room.adult;
        acc.push({
          sub_header: subHeader,
          name: "Adult",
          price: effectivePrice["TWN"],
          deposit: Big(effectivePrice["fixed_deposit"])
            .plus(
              Big(effectivePrice["TWN"])
                .times(effectivePrice["percentage_deposit"])
                .div(100)
            )
            .toFixed(2)
            .toString(),
          price_type: "TWN",
          quantity: room.adult,
        });
      }
      const totalChildren =
        (room.child_no_bed || 0) + (room.child_with_bed || 0);
      if (totalChildren > 0) {
        counter.CWB += totalChildren;
        acc.push({
          sub_header: subHeader,
          name: "Child",
          price: effectivePrice["CWB"],
          deposit: Big(effectivePrice["fixed_deposit"])
            .plus(
              Big(effectivePrice["CWB"])
                .times(effectivePrice["percentage_deposit"])
                .div(100)
            )
            .toFixed(2)
            .toString(),
          price_type: "CWB",
          quantity: totalChildren,
        });
      }

      return acc;
    }

    if (beds === 1) {
      pushLines("SGL", effectivePrice, subHeader, room, acc);
    } else if (beds === 2) {
      pushLines("TWN", effectivePrice, subHeader, room, acc);
    } else if (beds === 3) {
      pushLines("TRP", effectivePrice, subHeader, room, acc);
    } else {
      pushLines("QUAD", effectivePrice, subHeader, room, acc);
    }
    return acc;
  }, []);

  let totalPrice = invoiceItems.reduce(
    (acc, item) => acc.add(Big(item.price).times(item.quantity)),
    Big(0)
  );

  const items = rulesResult.surcharges.reduce((acc, surcharge) => {
    const price = Big(surcharge.price["fixed"])
      .plus(Big(surcharge.price["percentage"]).times(totalPrice).div(100))
      .toFixed(2);
    const line = {
      sub_header: "Adjustments",
      name: surcharge.name,
      price: price.toString(),
      deposit: price.toString(),
      price_type: "ADJUST",
      quantity: surcharge.qty,
    };
    acc.push(line);
    totalPrice = totalPrice.add(Big(price).times(surcharge.qty));
    return acc;
  }, invoiceItems);

  if (tourFormValue.tour_addons != null) {
    for (var value of tourFormValue.tour_addons) {
      const tour_addons = tour.tour_addons.find((x) => x.id === value.id);

      if (tour_addons == null) continue;
      const option = tour_addons.options.find((x) => x.id === value.option_id);

      if (option == null) {
        items.push({
          sub_header: tour_addons.name,
          name: tour_addons.default_option,
          price: "0",
          deposit: "0",
          price_type: "DEFAULT",
          quantity: "0",
        });
      } else if (notPricingByRoom) {
        if (counter.TWN > 0) {
          items.push({
            sub_header: tour_addons.name,
            name: option.name + " (TWN)",
            price: option.TWN,
            deposit: Big(effectivePrice["fixed_deposit"])
              .plus(
                Big(option.TWN)
                  .times(effectivePrice["percentage_deposit"])
                  .div(100)
              )
              .toFixed(2)
              .toString(),
            price_type: "TWN",
            quantity: counter.TWN,
          });
        }
        if (counter.CWB > 0) {
          items.push({
            sub_header: tour_addons.name,
            name: option.name + " (CWB)",
            price: option.CWB,
            deposit: Big(effectivePrice["fixed_deposit"])
              .plus(
                Big(option.CWB)
                  .times(effectivePrice["percentage_deposit"])
                  .div(100)
              )
              .toFixed(2)
              .toString(),
            price_type: "CWB",
            quantity: counter.CWB,
          });
        }
      } else {
        for (let price_type in counter) {
          const quantity = counter[price_type];
          if (quantity === 0) continue;
          items.push({
            sub_header: tour_addons.name,
            name: option.name + " (" + price_type + ")",
            price: option[price_type],
            deposit: Big(option[price_type])
              .times(effectivePrice["percentage_deposit"])
              .div(100)
              .toFixed(2)
              .toString(),
            price_type: price_type,
            quantity: quantity,
          });
        }
      }
    }
  }

  if (tourFormValue.generic_addons != null) {
    for (let generic_addon_form of tourFormValue.generic_addons) {
      const generic_addon = tour.generic_addons.find(
        (x) => x.id === generic_addon_form.id
      );
      if (generic_addon == null) continue;
      for (let option_form of generic_addon_form.options) {
        const quantity = parseInt("" + option_form.quantity);
        if (quantity === 0) continue;
        const option = generic_addon.options.find(
          (x) => x.id === option_form.id
        );
        items.push({
          sub_header: generic_addon.name,
          name: option.name,
          price: option.price,
          deposit: Big(option.price)
            .times(effectivePrice["percentage_deposit"])
            .div(100)
            .toFixed(2)
            .toString(),
          price_type: "",
          quantity: quantity,
        });
      }
    }
  }

  const toDate = new Date(tourFormValue.departure_date);
  toDate.setDate(toDate.getDate() + parseInt(optionSelected.duration, 10));

  const product = {
    product_source: "",
    product_source_ref: "",
    product_type: "tour_package",
    product_id: tour.id,
    product_code: optionSelected["product_code"],
    product_name: tour.name + " > " + optionSelected.name,
    product_category_ids: tour.categories.map((cat) => cat.id).join(","),
    quantity: traveler + "",
    from_date: tourFormValue.departure_date,
    to_date: dateToIsoDate(toDate),
    product_status: "Unconfirmed",
    adult,
    child,
    infant,
    items,
  };

  return {
    product,
    total: totalPrice.toString(),
  };
}

export type LegacyHotelForm = {
  hotelId: string;
  checkInDate: string;
  duration: number;
  adults: number;
  children: number;
};

export function computeGeneric(
  generic: Generic,
  params: {
    generic_id: string;
    option_id: string;
    quantity: number;
    from_date?: string;
    from_time?: string;
  }
): ComputeResult {
  params.quantity = parseInt(String(params.quantity || 0), 10);

  const option = generic.options.find(
    (optionx) => optionx.id === params.option_id
  );
  if (option == null) {
    return {
      error: "option_id " + params.option_id + " not found",
    };
  }

  const product: any = {
    product_name: generic.name,
    product_type: "nontour_product",
    product_status: "Unconfirmed",
    product_source: "",
    product_id: generic.id + "/" + option.id,
    product_code: option.code,
    product_category_ids: generic.categories.map((cat) => cat.id).join(","),
    quantity: params.quantity + "",
    items: [
      {
        sub_header: "",
        name: option.name,
        price: option.price,
        price_type: "",
        deposit: option.price,
        quantity: params.quantity + "",
      },
    ],
  };

  if (params.from_date != null) {
    product.from_date = params.from_date;
    if (params.from_time != null) {
      product.from_time = params.from_time;
    }
  }

  return {
    product,
    total: Big(option.price).times(params.quantity).toFixed(2),
  };
}

export type HotelbedsRoomRate = {
  rateKey: string;
  rateClass: string;
  rateType: string;
  net: string;
  travelcloud_price: string;
  allotment: number;
  rateCommentId: string;
  paymentType: string;

  packaging: boolean;
  boardCode: string;
  boardName: string;

  rooms: number;
  adults: number;
  children: number;

  cancellationPolicies: {
    amount: string;
    from: string;
  }[];

  // rateComments
  // taxes
  // rateBreakDown
  // offers
  // totalNet
  // currency
  // paymentDataRequired
};

export type HotelbedsRoom = {
  code: string;
  name: string;
  rates: HotelbedsRoomRate[];
};

export type HotelbedsBookingForm = {
  holder: {
    name: string;
    surname: string;
  };

  rooms: {
    rateKey: string;
    paxes: {
      roomId: string | number;
      type: string;
      name: string;
    }[];
  }[];
};

export type HotelbedsAvailability = {
  code: number;
  name: string;
  categoryCode: string;
  categoryName: string;
  destinationCode: string;
  destinationName: string;
  zoneCode: number;
  zoneName: string;
  latitude: string;
  longitude: string;
  rooms: HotelbedsRoom[];
};

export type HotelbedsCheckRateResponse = HotelbedsAvailability & {
  checkOut: string;
  checkIn: string;
};

// according to the best practice described by hotelbeds,
// more than one rateKey shouldn't send per request,
// @param hotel is expected to have only one room having only one rate
// Ref: https://developer.hotelbeds.com/docs/read/apitude_booking/Best_Practices
export function computeHotelbeds(
  data: { static: any; checkrates: HotelbedsCheckRateResponse },
  bookingForm: HotelbedsBookingForm
): ComputeResult {
  const hotel = data.checkrates;

  if (hotel == null || hotel.rooms.length !== 1)
    return {
      error: "Only expecting single room and rate for check rate",
    };

  // if (hotel == null || hotel.rooms.length !== 1 || hotel.rooms[ 0 ].rates.length !== 1) return {
  //   error: 'Only expecting single room and rate for check rate'
  // }

  // if (bookingForm == null || bookingForm.rooms.length !== 1) return {
  //   error: 'Only expecting single room and rate for booking form'
  // }

  // if (bookingForm.rooms[0].rateKey !== hotel.rooms[0].rates[0].rateKey) return {
  //   error: 'Ratekey in booking form is different from ratekey in check rate'
  // }

  const room = hotel.rooms[0];
  const rates = room.rates;
  const product_id = rates.map((rate) => rate.rateKey).join(",");
  const total = rates.reduce(
    (total: any, rate: any) => total.plus(rate.travelcloud_price),
    new Big(0)
  );
  const quantities = rates.reduce(
    (total: any, rate: any) => total + rate.rooms,
    0
  );

  const items = rates.map((rate) => {
    return {
      id: rate.rateKey,
      sub_header: hotel.name,
      price_rule_id: "",
      name: `${room.name} [${room.code}, ${rate.adults} adults]`,
      price: String(rate.travelcloud_price),
      cost: String(rate.net),
      price_type: "",
      deposit: String(rate.travelcloud_price),
      quantity: String(rate.rooms),
    };
  });

  const product = {
    product_name: hotel.name,
    product_type: "hotel_room",
    product_status: "Unconfirmed",
    product_source: "hotelbeds",
    product_id,
    product_code: room.code,
    product_detail: data,
    from_date: hotel.checkIn,
    to_date: hotel.checkOut,
    quantity: String(quantities),
    items,
  };

  return {
    product,
    total: total.toFixed(),
  };
}

export function computeGlobaltix(
  item,
  form: GlobaltixFormValue
): ComputeResult {
  const quantity = parseInt(String(form.quantity || 0), 10);

  if (item.ticketTypes != null) {
    const ticket = item.ticketTypes.find(
      (ticket) => String(ticket.id) === String(form.id)
    );
    if (ticket == null) {
      return {
        error: "ticketType_id " + item.ticketType_id + " not found",
      };
    }

    var name = [ticket.name];
    if (form.visitDate != null) name.push("- Visit date: " + form.visitDate);
    if (form.questionList != null) {
      for (var answerIndex in form.questionList) {
        const answer = form.questionList[answerIndex];
        name.push("- " + answer.answer);
      }
    }

    const product = {
      product_name: item.title,
      product_type: "attraction",
      product_status: "Unconfirmed",
      product_source: "globaltix",
      product_id: String(item.id) + "/" + String(form.id),
      product_code: "",
      product_detail: item,
      quantity: quantity + "",
      items: [
        {
          sub_header: "",
          name: name.join("\n"),
          price: String(ticket.travelcloud_price),
          cost: String(ticket.travelcloud_cost),
          price_type: "",
          deposit: String(ticket.travelcloud_price),
          quantity: quantity + "",
        },
      ],
    };

    return {
      product,
      total: Big(ticket.travelcloud_price).times(quantity).toFixed(2),
    };
  } else if (item.attraction != null) {
    var name = [item.name];
    if (form.visitDate != null) name.push("- Visit date: " + form.visitDate);
    if (form.questionList != null) {
      for (var answerIndex in form.questionList) {
        const answer = form.questionList[answerIndex];
        name.push("- " + answer.answer);
      }
    }

    const product = {
      product_name: item.attraction.title,
      product_type: "attraction",
      product_status: "Unconfirmed",
      product_source: "globaltix",
      product_id: String(item.attraction.id) + "/" + String(form.id),
      product_code: "",
      quantity: quantity + "",
      items: [
        {
          sub_header: "",
          name: name.join("\n"),
          price: String(item.travelcloud_price),
          cost: String(item.travelcloud_cost),
          price_type: "",
          deposit: String(item.travelcloud_price),
          quantity: quantity + "",
        },
      ],
    };

    return {
      product,
      total: Big(item.travelcloud_price).times(quantity).toFixed(2),
    };
  } else {
    return {
      error:
        "Item is in unknown format. Please provide attraction with ticketTypes from attraction/list or ticketType/get",
    };
  }
}

function segmentToString(segment: FlightDetail["od1"]["segments"][0]): string {
  const flights_info = getFlightsSummary(segment);

  return (
    segment.marketing_carrier.code +
    segment.flight_number +
    " " +
    flights_info.origin.code +
    " (" +
    flights_info.origin_datetime.substr(0, 16) +
    ")" +
    " ==> " +
    flights_info.destination.code +
    " (" +
    flights_info.destination_datetime.substr(0, 16) +
    ")"
  );
}

function getFlightsSummary(segment: FlightDetail["od1"]["segments"][0]): {
  origin: { code: string };
  origin_datetime: string;
  destination: { code: string };
  destination_datetime: string;
} {
  if (segment.flights_info != null) return segment.flights_info;
  return {
    origin: segment.flights[0].departure_airport,
    origin_datetime: segment.flights[0].departure_datetime,
    destination: segment.flights[segment.flights.length - 1].arrival_airport,
    destination_datetime:
      segment.flights[segment.flights.length - 1].arrival_datetime,
  };
}

export function computeFlight(flight: FlightDetail, form: FlightFormValue) {
  const selectedFare = flight.pricings.find((x) => x.id === form.pricing_id);
  if (selectedFare == null)
    return { error: "fare " + form.pricing_id + "not found" };

  const product: any = {};
  product.product_type = "flight";
  product.product_source = selectedFare.source;
  product.product_status = "Unconfirmed";
  product.adult = selectedFare.ptc_breakdown_map["adt"].quantity;
  product.child =
    selectedFare.ptc_breakdown_map["cnn"] != null
      ? selectedFare.ptc_breakdown_map["cnn"].quantity
      : 0;
  product.infant =
    selectedFare.ptc_breakdown_map["inf"] != null
      ? selectedFare.ptc_breakdown_map["inf"].quantity
      : 0;
  product.quantity = product.adult + product.child;
  product.product_detail = flight;

  const od1FirstSegment = getFlightsSummary(flight.od1.segments[0]);
  const od1LastSegment = getFlightsSummary(
    flight.od1.segments[flight.od1.segments.length - 1]
  );

  product.product_name =
    od1FirstSegment.origin.code + " -> " + od1LastSegment.destination.code;

  if (flight.od2) {
    const od2FirstSegment = getFlightsSummary(flight.od2.segments[0]);
    const od2LastSegment = getFlightsSummary(
      flight.od2.segments[flight.od2.segments.length - 1]
    );

    if (od1LastSegment.destination.code === od2FirstSegment.origin.code) {
      product.product_name += " -> " + od2LastSegment.destination.code;
    } else {
      product.product_name +=
        " / " +
        od2FirstSegment.origin.code +
        " -> " +
        od2LastSegment.destination.code;
    }

    product.from_date = od1FirstSegment.origin_datetime.substr(0, 16);
    product.to_date = od2LastSegment.destination_datetime.substr(0, 16);
  } else {
    product.from_date = od1FirstSegment.origin_datetime.substr(0, 16);
    product.to_date = od1LastSegment.destination_datetime.substr(0, 16);
  }

  let segmentsFlattened = flight.od1.segments;

  if (flight.od2 != null) {
    segmentsFlattened = segmentsFlattened.concat(flight.od2.segments);
  }

  const subhead = segmentsFlattened
    .map((x: any) => segmentToString(x))
    .join("\n");

  product.items = [];

  let total = Big(0);

  for (const ptc in selectedFare.ptc_breakdown_map) {
    if (selectedFare.ptc_breakdown_map.hasOwnProperty(ptc)) {
      const breakdown = selectedFare.ptc_breakdown_map[ptc];
      // const tax = formatCurrency(breakdown.tax)
      product.items.push({
        header: "Item",
        sub_header: subhead,
        name: ptc.toUpperCase(),
        price: breakdown.price,
        cost: breakdown.cost,
        deposit: breakdown.price,
        quantity: breakdown.quantity,
      });
      const subTotal = Big(breakdown.price).times(breakdown.quantity);
      total = total.add(subTotal);
    }
  }

  if (total.eq(selectedFare.price) === false) {
    return {
      error: `Error in fare. Breakdown: ${total.toFixed(2)}. Total: ${
        selectedFare.price
      }`,
    };
  }

  return {
    total: total.toFixed(2),
    product,
  };
}

function dateToIsoDate(date: Date): string {
  const pad = (x: number) => (x < 10 ? "0" + x : x);
  return (
    "" +
    date.getFullYear() +
    "-" +
    pad(date.getMonth() + 1) +
    "-" +
    pad(date.getDate())
  );
}

function formatCurrency(x: string | number): string {
  const xNumber = typeof x === "string" ? parseFloat(x) : x;
  const neg = xNumber < 0 ? "-" : "";
  return neg + "$" + Big(x).abs().toFixed(2).toString();
}

// not null not empty (not undefined)
// TravelCloud treats null and empty values identically
function nnne(x: string): boolean {
  return x != null && x.length > 0;
}

// not null not zero (for checking boolean fields)
function nnnz(x: string): boolean {
  return x != null && x.length > 0 && parseInt(x, 10) !== 0;
}

export function filterPriceRulesWithCheckoutCode(
  checkoutCode: string,
  priceRules: PriceRules,
  bcryptCache: { [key: string]: boolean },
  customer: Customer
): PriceRules {
  if (checkoutCode == null) return [];

  const priceRulesCombined =
    customer == null ||
    customer.categories == null ||
    customer.categories[0] == null ||
    customer.categories[0].customer_in_any_price_rules == null
      ? priceRules
      : customer.categories.reduce(
          (acc, category) => acc.concat(category.customer_in_any_price_rules),
          priceRules.slice(0)
        );

  if (priceRulesCombined == null) return [];

  return priceRulesCombined.filter((rule) => {
    if (nnne(rule.condition_checkout_code_hash)) {
      const cacheKey = rule.condition_checkout_code_hash + "/" + checkoutCode;
      if (bcryptCache[cacheKey] == null)
        bcryptCache[cacheKey] = bcrypt.compareSync(
          checkoutCode,
          rule.condition_checkout_code_hash
        );

      return bcryptCache[cacheKey];
    }
    return false;
  });
}

// "Deposit Unpaid", "Fully Paid", "Deposit Paid"
export function computePriceRules(
  order: Order,
  priceRules: PriceRules,
  bcryptCache: { [key: string]: boolean },
  customer: Customer
): Order {
  // remove previous computations if any
  order.products = order.products.map((product) => {
    product.items = product.items.filter(
      (item) =>
        !nnnz(item.price_rule_id) && !nnnz((item as any).is_from_price_rule)
    );
    return product;
  });

  if (priceRules == null) priceRules = [];

  const orderTotal = order.products
    .reduce((acc, product) => {
      const itemsTotal = product.items.reduce(
        (acc2, item) => acc2.plus(Big(item.price).times(item.quantity)),
        Big(0)
      );
      return acc.plus(itemsTotal);
    }, Big(0))
    .toFixed(2);

  const priceRulesCombined =
    customer == null ||
    customer.categories == null ||
    customer.categories[0] == null ||
    customer.categories[0].customer_in_any_price_rules == null
      ? priceRules
      : customer.categories.reduce(
          (acc, category) => acc.concat(category.customer_in_any_price_rules),
          priceRules.slice(0)
        );

  var priceRulesSorted: PriceRuleWithCount[] = priceRulesCombined.sort((rule) =>
    parseInt(rule.adjustment_sequence, 10)
  );

  //initialize _order_use_count_quantity by mutation
  priceRulesSorted = priceRulesSorted.map((val) => {
    val._order_use_count_quantity = 0;
    return val;
  });

  const now = new Date();
  var travelersCount;

  if (order.travelers == null) {
    const maxAdult = order.products.reduce(
      (acc, product) =>
        (product as any).adult != null
          ? Math.max(parseInt((product as any).adult), acc)
          : acc,
      0
    );
    const maxChild = order.products.reduce(
      (acc, product) =>
        (product as any).child != null
          ? Math.max(parseInt((product as any).child), acc)
          : acc,
      0
    );
    const maxInfant = order.products.reduce(
      (acc, product) =>
        (product as any).infant != null
          ? Math.max(parseInt((product as any).infant), acc)
          : acc,
      0
    );
    travelersCount = maxAdult + maxChild + maxInfant;
  } else {
    travelersCount = order.travelers.length;
  }

  // compute price attributes for
  for (const product of order.products) {
    const mutexDict = {};
    const ruleIdDict = {};
    const total = product.items.reduce(
      (acc, item) => acc.add(Big(item.price).times(item.quantity)),
      Big(0)
    );
    let running = Big(total); // Clone. Big doesn't seem to mutate it's object so this might not be required.
    let seqTotal = Big(0);
    let lastSeq;

    const priceAttributes = Object.assign(
      {
        "order/total": orderTotal,
        "product/total": product.items.reduce(
          (acc, item) => acc.plus(Big(item.price).times(item.quantity)),
          Big(0)
        ),
        "product/quantity": product.quantity,
        "travelers/count": travelersCount,
      },
      product.product_attributes
    );

    for (const rule of priceRulesSorted) {
      if (ruleIdDict[rule.id] != null) continue;
      // console.log('processing rule ' + rule.id)
      ruleIdDict[rule.id] = true;

      if (lastSeq != rule.adjustment_sequence) {
        running = running.add(seqTotal);
        seqTotal = Big(0);
      }
      lastSeq = rule.adjustment_sequence;

      if (
        nnne(rule.condition_product_source) &&
        rule.condition_product_source !== product.product_source
      ) {
        // console.log('rule ' + rule.id + ' condition_product_source check failed')
        continue;
      }

      if (nnne(rule.condition_product_type)) {
        if (rule.condition_product_type !== product.product_type) {
          // console.log('rule ' + rule.id + ' condition_product_type check failed')
          continue;
        }
      }

      if (nnne(rule.condition_from_datetime)) {
        const condition_from_date = new Date(rule.condition_from_datetime);
        if (isNaN(condition_from_date.getTime()) || condition_from_date > now) {
          // console.log('rule ' + rule.id + ' condition_from_datetime check failed')
          continue;
        }
      }

      if (nnne(rule.condition_to_datetime)) {
        const condition_to_date = new Date(rule.condition_to_datetime);
        if (isNaN(condition_to_date.getTime()) || condition_to_date < now) {
          // console.log('rule ' + rule.id + ' condition_to_datetime check failed')
          continue;
        }
      }

      if (
        nnnz(rule.condition_minimum_product_quantity) &&
        Big(rule.condition_minimum_product_quantity).gt(
          priceAttributes["product/quantity"]
        )
      ) {
        // console.log('rule ' + rule.id + ' condition_minimum_product_quantity check failed')
        continue;
      }

      if (rule.is_public === "0") {
        if (customer == null) continue;
        if (
          rule.condition_customer_in_any_categories.find(
            (category1) =>
              customer.categories.find(
                (category2) => category1.id === category2.id
              ) != null
          ) == null
        )
          continue;
      }

      if (rule.condition_product_in_any_categories_enabled === "1") {
        if (product.product_category_ids == null) {
          // console.log('rule ' + rule.id + ' condition_product_in_any_categories check failed')
          continue;
        }
        const product_category_ids = product.product_category_ids.split(",");

        if (
          rule.condition_product_in_any_categories.find(
            (category1) =>
              product_category_ids.find(
                (category2) => category1.id === category2
              ) != null
          ) == null
        ) {
          // console.log('rule ' + rule.id + ' condition_product_in_any_categories check failed')
          continue;
        }
      }

      if (nnne(rule.condition_checkout_code_hash)) {
        if (!nnne(order.checkout_code)) {
          // console.log('rule ' + rule.id + ' condition_checkout_code_hash check failed')
          continue;
        }

        const cacheKey =
          rule.condition_checkout_code_hash + "/" + order.checkout_code;
        if (bcryptCache[cacheKey] == null)
          bcryptCache[cacheKey] = bcrypt.compareSync(
            order.checkout_code,
            rule.condition_checkout_code_hash
          );

        if (bcryptCache[cacheKey] === false) {
          // console.log('rule ' + rule.id + ' condition_checkout_code_hash check failed')
          continue;
        }
      }

      // this sets mutexDict and needs to be the last check
      if (nnne(rule.condition_mutex_code)) {
        if (mutexDict[rule.condition_mutex_code] != null) {
          // console.log('rule ' + rule.id + ' condition_mutex_code check failed')
          continue;
        }
        mutexDict[rule.condition_mutex_code] = true;
      }

      if (
        rule.condition_maximum_use_count_quantity != null &&
        parseInt(rule.use_count_quantity, 10) +
          parseInt(product.quantity, 10) +
          rule._order_use_count_quantity >
          parseInt(rule.condition_maximum_use_count_quantity, 10)
      ) {
        // console.log('rule ' + rule.id + ' condition_maximum_use_count_quantity check failed')
        continue;
      }

      // console.log('rule ' + rule.id + ' passed')

      let adjustmentTotal = Big(0);

      // calculate adjustment per unit
      // as product_id is only unique within a product source, we require condition_product_source to be set
      const adjustmentLine =
        !nnne(rule.condition_product_source) ||
        !nnne(rule.adjustment_product_id_final_per_unit_csv)
          ? null
          : rule.adjustment_product_id_final_per_unit_csv
              .split("\n")
              .find((line) => line.split(",")[0].trim() === product.product_id);

      if (adjustmentLine != null) {
        const split = adjustmentLine.split(",");
        if (split.length > 1) {
          try {
            adjustmentTotal = adjustmentTotal.plus(
              Big(split[1].trim()).times(product.quantity)
            );
          } catch (e) {}
        }
      } else {
        if (nnnz(rule.adjustment_per_unit)) {
          adjustmentTotal = adjustmentTotal.plus(
            Big(rule.adjustment_per_unit).times(product.quantity)
          );
        }

        // calculate adjustment percentage
        let priceAttribute = Big(0);

        if (
          rule.adjustment_percentage_price_attribute === "product_running_total"
        ) {
          priceAttribute = running;
        } else if (
          priceAttributes[rule.adjustment_percentage_price_attribute] != null
        ) {
          priceAttribute = Big(
            priceAttributes[rule.adjustment_percentage_price_attribute]
          );
        }

        if (!priceAttribute.eq(0)) {
          let adjustmentPercentage = nnne(rule.adjustment_percentage)
            ? Big(rule.adjustment_percentage).times(priceAttribute).div(100)
            : Big(0);

          adjustmentTotal = adjustmentTotal.plus(adjustmentPercentage);
        }
      }

      seqTotal = seqTotal.plus(adjustmentTotal);
      rule._order_use_count_quantity += parseInt(product.quantity, 10);

      if (adjustmentTotal.eq("0") === false) {
        product.items.push({
          id: undefined,
          header: "",
          sub_header: "Adjustments",
          name: rule.adjustment_name,
          price: adjustmentTotal.toFixed(2),
          price_type: rule.adjustment_price_type,
          deposit: adjustmentTotal.toFixed(2), // FIXME: percentage_deposit should be found on product level
          quantity: "1",
          price_rule_id: rule.id,
        });
      }
    }
  }
  return order;
}

function urldecode(str) {
  return JSON.parse(
    `{"${str.replace(/&/g, '","').replace(/\=/g, '":"')}"}`,
    function (key, value) {
      if (key === "") {
        return value;
      } else {
        return decodeURIComponent(value);
      }
    }
  );
}

export function computeLegacyHotel(details, checkout: LegacyHotelFormValue) {
  const source = "bedsonline";
  const room = details.rooms.find((room) => room.id === checkout.room_id);

  if (room == null) {
    return {
      error: "Cannot find room id " + checkout.room_id,
    };
  }

  const param = checkout.params;

  const { duration } = param;
  const { checkInDate } = param;
  const checkOutDate = moment(param.checkInDate, "YYYY-MM-DD")
    .add(param.duration, "days")
    .format("YYYY-MM-DD");

  const product: any = {};
  product.items = [];
  product.product_name = details.name;
  product.product_type = "hotel_room";
  product.product_status = "Unconfirmed";
  product.product_source = source;
  product.product_id = room.id;
  product.to_date = checkOutDate;
  product.from_date = checkInDate;
  product.adult = 1;
  product.quantity = "1";

  const voucherRoom = JSON.parse(JSON.stringify(room));
  voucherRoom.prices = [];

  const price = room.prices.find((price) => price.code === checkout.price_code);

  if (price == null) {
    return {
      error: "Cannot find price code " + checkout.price_code,
    };
  }

  product.items.push({
    header: "Item",
    sub_header: room.description,
    name: `Check-in on ${checkInDate}, Check-out on ${checkOutDate}, ${duration} nights`,
    price: price.price,
    price_type: price.code,
    deposit: price.price,
    cost: price.cost,
    cost_source: source,
    quantity: "1",
  });

  if (price.cost != null) {
    delete price.cost;
  }
  price.quantity = "1";
  voucherRoom.prices.push(price);

  //capture info for voucher
  product.voucher = {
    contact: details.contact,
    latitude: details.latitude,
    longitude: details.longitude,
    remarks: details.remarks,
    room: voucherRoom,
  };

  return { product };
}
