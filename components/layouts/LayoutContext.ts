import { createContext } from "react";
import { Cart, TravelCloudClient } from "../../core/travelcloud";
import { Customer, Order } from "../../core/types";

const LayoutContext: any = createContext({});

export default LayoutContext;
