import React, { useEffect, useState } from "react";
import { Button, Drawer, Layout, Menu, notification } from "antd";
import Link from "next/link";
import styles from "../../styles/SiderLayout.module.scss";
import config from "../../customize/config";
import { Cart, setupGtag, TravelCloudClient } from "../../core/travelcloud";
import Router from "next/router";
import { CloseOutlined } from "@ant-design/icons";
import { Order } from "../tc-domain/order/Order";
import Account from "../tc-domain/accounts/Account";
import LayoutContext from "./LayoutContext";

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

const SiderLayout = ({ children, priceRules }: any) => {
  const [collapsed, setCollapsed] = useState(false);
  const onCollapse = (collapsed: any) => {
    setCollapsed(collapsed);
  };

  const [windowSize, setWindowSize] = useState<any>({});
  const [adminMode, setAdminMode] = useState<boolean>(false);
  const [sider, setSider] = useState<string>("");
  const [order, setOrder] = useState<any>({ loading: true });
  const [customer, setCustomer] = useState<any>({ loading: true });
  const [, setPriceRules] = useState<any>({ loading: true });
  const [cart, setCart] = useState<any>(null);

  const client: TravelCloudClient = new TravelCloudClient(config.tcUser);

  useEffect(() => {
    if (window.location.pathname === "/admin") {
      window.location.href = `https://${config.tcUser}.travelcloud.app/admin/en`;
    }

    setupGtag(Router, config);

    // cookies notification
    if (localStorage.getItem("setted") !== "1") {
      openNotification();
    }

    setCart(
      new Cart({
        client,
        priceRules: priceRules,
        onOrderChange: (order) => setOrder(order),
        onCustomerChange: (customer) => setCustomer(customer),
        onPriceRulesChange: (priceRules) => setPriceRules(priceRules),
      })
    );

    document.addEventListener("keydown", (event) => {
      if (event.ctrlKey && event.code === "Backquote") {
        setAdminMode(!adminMode);
      }
    });

    checkWindowSize();
    window.addEventListener("resize", checkWindowSize);
  }, []);

  const openNotification = () => {
    notification.open({
      duration: 4.5,
      btn: null,
      placement: "bottomLeft",
      message: (
        <small>
          We use cookies to improve our website information and services. By
          using our site, you consent to cookies as outlined in our{" "}
          <Link href="/document/privacy-policy#cookies">
            <a>Privacy Policy</a>
          </Link>
          .
        </small>
      ),
    });
    localStorage.setItem("setted", "1");
  };

  const checkWindowSize = () => {
    setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight,
    });
  };

  const openCart = async () => {
    setSider("cart");
  };

  const closeCart = () => {
    setSider("");
  };

  const cartIsEmpty =
    order.result == null ||
    order.result.products == null ||
    order.result.products.length === 0;

  return (
    <LayoutContext.Provider
      value={{
        windowSize,
        adminMode,
        cart,
        customer,
        openCart,
        client,
        order,
      }}
    >
      <Layout className={styles.whole_layout}>
        <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
          <div className={styles.logo} />
          <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
            <SubMenu key={"home"} icon={""} title={"Home"}>
              <Menu.Item key={""}>
                <Link
                  href={{
                    pathname: "/",
                    query: { parent: "", name: "" },
                  }}
                >
                  <a>Flight</a>
                </Link>
              </Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
        <Layout className={styles.site_layout}>
          <Header className={styles.site_layout_header} />
          <Content className={styles.site_content}>
            <div className={styles.site_layout_background}>{children}</div>
            <Drawer
              className="cart-drawer"
              visible={!!sider}
              closable={false}
              onClose={closeCart}
              width={windowSize.width < 992 ? "100%" : "50%"}
            >
              <div style={{ textAlign: "right" }}>
                <CloseOutlined
                  onClick={closeCart}
                  style={{ cursor: "pointer" }}
                />
              </div>

              {sider === "cart" && (
                <>
                  <h3 className="color-primary">
                    <strong>Shopping Cart</strong>
                  </h3>

                  {cartIsEmpty ? (
                    <div>Your cart is empty</div>
                  ) : (
                    <>
                      <Order
                        order={order.result}
                        showSection={{ products: true, remove: true }}
                        cart={cart}
                      />
                      <Button
                        type="primary"
                        size="large"
                        onClick={() => {
                          setSider("");
                          Router.push("/checkout");
                        }}
                      >
                        Proceed to checkout
                      </Button>
                    </>
                  )}
                </>
              )}

              {sider === "account" && (
                <Account customer={customer} cart={cart} />
              )}
            </Drawer>
          </Content>
          <Footer style={{ textAlign: "center" }}>
            Ant Design ©2018 Created by Ant UED
          </Footer>
        </Layout>
      </Layout>
    </LayoutContext.Provider>
  );
};

export default SiderLayout;
