import { MenuOutlined } from "@ant-design/icons";
import Account from "./Account";

export default {
  name: "Account",
  icon: <MenuOutlined />,
  types: {
    Account,
  },
};
