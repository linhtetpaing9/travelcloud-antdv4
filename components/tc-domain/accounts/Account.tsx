import { LoadingOutlined, LoginOutlined, LogoutOutlined, MailOutlined } from "@ant-design/icons";
import { Avatar, Button, Card, Form, Input, Spin, Table, Tabs, Tag } from "antd";
import moment from "moment";
import React, { useState } from "react";
import { ObjectLiteral } from "./types";
import Big from "big.js";

const Account = ({ cart, customer }) => {
  const [loginLoading, setLoginLoading] = useState<boolean>(false);
  const [loginRequested, setLoginRequested] = useState<number>(0);
  const [loginVerifyStatus, setLoginVerifyStatus] = useState<ObjectLiteral>({});
  const [loginEmail, setLoginEmail] = useState<string>("");
  const [loginVerify, setLoginVerify] = useState<string>("");

  const getPassword = async () => {
    setLoginLoading(true);
    await cart.getPassword(loginEmail);
    setLoginLoading(false);
    setLoginRequested(Date.now());
  };

  const logout = async () => {
    cart.logout();
  };

  const login = async () => {
    setLoginLoading(true);
    setLoginVerifyStatus({});

    const customer = await cart.login(loginEmail, loginVerify);
    if (customer.result == null) {
      setLoginLoading(false);
      setLoginVerify("");
      setLoginVerifyStatus({
        validateStatue: "error",
        help: "Invalid password",
      });
    } else {
      setLoginLoading(false);
    }
  };

  return (
    <div>
      <div style={{ fontSize: 24, padding: "24px 0" }}>
        My Account{" "}
        {customer.result != null && (
          <span
            style={{
              color: "#999",
              fontSize: 14,
              marginLeft: 16,
              cursor: "pointer",
            }}
            onClick={() => logout()}
          >
            <LogoutOutlined /> Logout
          </span>
        )}
      </div>
      {customer.result == null && loginLoading == true && (
        <div>
          <Spin
            size="large"
            style={{ display: "block", margin: "64px auto" }}
          />
        </div>
      )}
      {customer.result == null &&
        loginLoading == false &&
        (Date.now() - loginRequested > 600000 ? (
          <div>
            <div>We will send a temporary password to your email</div>
            <Form>
              <Form.Item>
                <Input
                  placeholder="E-mail"
                  onChange={(e) => setLoginEmail(e.target.value)}
                  onPressEnter={() => getPassword()}
                />
              </Form.Item>
              <Form.Item>
                <Button
                  size="large"
                  type="primary"
                  disabled={loginLoading}
                  onClick={() => getPassword()}
                >
                  {loginLoading ? <LoginOutlined /> : <MailOutlined />}
                  Send email
                </Button>
              </Form.Item>
            </Form>
          </div>
        ) : (
          <div>
            <div>Please check your email for your password</div>
            <Form>
              <Form.Item>
                <Input disabled value={loginEmail} />
              </Form.Item>
              <Form.Item {...loginVerifyStatus}>
                <Input
                  placeholder="password"
                  onChange={(e) => setLoginVerify(e.target.value)}
                  onPressEnter={() => login()}
                />
              </Form.Item>
              <Form.Item>
                <Button
                  size="large"
                  type="primary"
                  disabled={loginLoading}
                  onClick={() => login()}
                >
                  {loginLoading ? <LoadingOutlined /> : <LoginOutlined />}
                  Login
                </Button>
              </Form.Item>
            </Form>
          </div>
        ))}
      {customer.result != null && (
        <div>
          <Card style={{ border: 0 }}>
            <Card.Meta
              avatar={
                <Avatar
                  size="large"
                  icon="user"
                  style={{ backgroundColor: "#af0201" }}
                />
              }
              title={
                <span>
                  <span style={{ marginRight: 8 }}>
                    {customer.result.name
                      ? customer.result.name
                      : customer.result.email}
                  </span>
                  {customer.result.categories.map((cat) => (
                    <Tag key={cat.id}>{cat.name}</Tag>
                  ))}
                </span>
              }
              description={
                <span>
                  Account balance $
                  {customer.result.payments
                    .reduce((acc, payment) => acc.plus(payment.amount), Big(0))
                    .times(-1)
                    .toFixed(2)
                    .toString()}
                </span>
              }
            />
          </Card>
          <Tabs defaultActiveKey="1">
            <Tabs.TabPane tab="Orders" key="1">
              <Table
                columns={[
                  {
                    title: "Date",
                    render: (x) => (
                      <span>{moment(x.order_date).format("Do MMMM YYYY")}</span>
                    ),
                    sorter: (x, y) =>
                      parseInt((x as any).id) - parseInt((y as any).id),
                    sortOrder: "descend",
                    key: "order_date",
                  },
                  {
                    title: "Ref",
                    render: (x) => (
                      <a href={"/payment?ref=" + x.ref}>{x.ref}</a>
                    ),
                    key: "ref",
                  },
                  {
                    title: "Amount",
                    dataIndex: "payment_required",
                    key: "payment_required",
                  },
                  {
                    title: "Status",
                    dataIndex: "order_status",
                    key: "order_status",
                  },
                  {
                    title: "Payment",
                    dataIndex: "payment_status",
                    key: "payment_status",
                  },
                ]}
                dataSource={customer.result.orders}
                rowKey="id"
              />
            </Tabs.TabPane>
            <Tabs.TabPane tab="Statement" key="2">
              <Table
                columns={[
                  {
                    title: "Date",
                    render: (x) => (
                      <span>{moment(x.date_added).format("Do MMMM YYYY")}</span>
                    ),
                    sorter: (x, y) =>
                      parseInt((x as any).id) - parseInt((y as any).id),
                    sortOrder: "descend",
                    key: "date_added",
                  },
                  {
                    title: "Ref",
                    render: (x) =>
                      x.order == null ? (
                        "N/A"
                      ) : (
                        <a href={"/payment?ref=" + x.order.ref}>
                          {x.order.ref}
                        </a>
                      ),
                    key: "order.ref",
                  },
                  {
                    title: "Description",
                    dataIndex: "description",
                    key: "description",
                  },
                  {
                    title: "Amount",
                    dataIndex: "amount",
                    key: "amount",
                  },
                ]}
                dataSource={customer.result.payments}
                rowKey="id"
              />
            </Tabs.TabPane>
          </Tabs>
        </div>
      )}
    </div>
  );
};

export default Account;
