import { MenuOutlined } from "@ant-design/icons";
import FlightTemplate from "./FlightTemplate";

export default {
  name: "FlightTemplate",
  icon: <MenuOutlined />,
  types: {
    FlightTemplate,
  },
};
