import Flights from "./flights";
import Accounts from "./accounts";
import Order from "./order";

export default {
  Flights,
  Accounts,
  Order
};
