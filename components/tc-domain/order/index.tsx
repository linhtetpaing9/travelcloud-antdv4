import { MenuOutlined } from "@ant-design/icons";
import { Order } from "./Order";

export default {
  name: "Order",
  icon: <MenuOutlined />,
  types: {
    Order,
  },
};
