import { MenuOutlined } from "@ant-design/icons";
import TourTemplate from "./TourTemplate";

export default {
  name: "TourTemplate",
  icon: <MenuOutlined />,
  types: {
    TourTemplate,
  },
};
