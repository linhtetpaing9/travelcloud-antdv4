/* eslint-disable @next/next/no-img-element */
/* eslint-disable react/display-name */
/* eslint-disable import/no-anonymous-default-export */
import React from 'react'
import Image from 'next/image'
import { Row, Col, Carousel, FormInstance } from 'antd'
import { TravelCloudClient, TcResponse, Cart, extractValueFromFormState } from '../../../core/travelcloud'
import {TourBookingForm, initialTourBookingForm} from './src/tour-booking'
import Router from 'next/router'
import { Tour } from '../../../core/types'
import { PrintDiv } from './src/print-div';
import config from '../../../customize/config'

export default class extends React.PureComponent<{tour: TcResponse<Tour>, cart: Cart, form: FormInstance}> {
  state = {tourBookingForm: null}
  static getDerivedStateFromProps(props, currentState) {
    if (props.tour.result != null && currentState.tourBookingForm == null) {
      return {
        tourBookingForm: initialTourBookingForm(props.tour.result)
      }
    }
    return null
  }

  render() {
    const tour = this.props.tour.result
    console.log({tour})

    if (tour == null) return <div>Tour not found</div>

    return (
      <div>
        <Row style={{margin: '32px 64px'}}>
          <h1 style={{fontSize: 40, fontWeight: 'normal', marginBottom: 0}}>{tour.name}</h1>
        </Row>
        <Row style={{backgroundColor: '#fff', margin: '32px 64px', height: '650px'}}>
          <Col span={12}>
            <Carousel autoplay>
              <div key='cover'><div style={{
                backgroundImage: `url(${tour.photo_url})`,
                height: '650px',
                width: '100%',
                backgroundSize: 'cover',
                backgroundPosition: 'center'}} /></div>
              {tour.photos.map((photo, index) =>
                <div key={index}><div style={{
                  backgroundImage: `url(${photo.url})`,
                  height: '650px',
                  width: '100%',
                  backgroundSize: 'cover',
                  backgroundPosition: 'center'}} /></div>)}
            </Carousel>
          </Col>
          <Col span={12} style={{height: '100%'}}>
            <TourBookingForm
              cart={this.props.cart}
              value={this.state.tourBookingForm}
              tour={tour}
              form={this.props.form}
              onChange={(tourBookingForm) => this.setState({tourBookingForm})}
              onSubmit={() => {
                this.props.cart.reset().addTour(tour, this.state.tourBookingForm)
                Router.push('/checkout')
              }} />
          </Col>
        </Row>
        <PrintDiv printDivId="printPhoto" hideOriginalDiv={true}>
          <h2 style={{fontSize: 40, fontWeight: 'normal', marginBottom: 0}}>{tour.name}</h2>
          <img src={tour.photo_url} style={{display: 'block', margin: "20px auto"}} alt="Tour Image" />
        </PrintDiv>
        <PrintDiv printDivId="printItinerary">
          <Row gutter={64} style={{backgroundColor: '#fff', margin: '32px 64px', padding: 32}}>
            <Col span={12}>
              <h2>Highlights</h2>
              <div dangerouslySetInnerHTML={{__html: tour.short_desc}} />
              <h2>Extras</h2>
              <div dangerouslySetInnerHTML={{__html: tour.extras}} />
              <h2>Remarks</h2>
              <div dangerouslySetInnerHTML={{__html: tour.remarks}} />
            </Col>
            <Col span={12}>
              <h2>Itinerary</h2>
              <div dangerouslySetInnerHTML={{__html: tour.itinerary}} />
            </Col>
          </Row>
        </PrintDiv>
      </div>
    )
  }
}
