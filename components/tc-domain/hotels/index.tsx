import { MenuOutlined } from "@ant-design/icons";
import HotelTemplate from "./HotelTemplate";

export default {
  name: "HotelTemplate",
  icon: <MenuOutlined />,
  types: {
    HotelTemplate,
  },
};
