import {useState, useEffect} from 'react';

export default (results, perPage) => {
  const [currentPage, setCurrentPage] = useState(1);
  const [resultsToRender, setResultsToRender] = useState([]);

  const getSlicedData = (page) => {
    return results.slice((page - 1) * perPage, page * perPage);
  }

  useEffect(() => {
    setResultsToRender(getSlicedData(currentPage));
  }, [results]);

  const loadMore = () => {
    setResultsToRender([...resultsToRender, ...getSlicedData(currentPage + 1)])
    setCurrentPage(currentPage + 1);
  };

  const isLastPage = () => {
    return Math.ceil( results.length / perPage ) == currentPage
  }

  return {
    resultsToRender,
    loadMore,
    currentPage,
    isLastPage
  }
}