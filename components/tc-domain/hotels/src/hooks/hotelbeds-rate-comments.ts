import { useState } from "react"
import { TravelCloudClient } from "../../../../../core/travelcloud";
import { loadRateComments, parseAndFilterRateComments } from "../../../../pure/Voucher";

export const useHotelbedsRateComments = ({ hotel, rates, client }: 
  { hotel: any, rates: any[], client: TravelCloudClient }) => {

  const [ states, setStates ] = useState({
    rateComments: []
  })

  const fetchRateComments = () => {
    const resp = loadRateComments(hotel.code, rates, client)
    resp != null && resp.then(rateComments => setStates(prev => {
      return Object.assign({}, prev, { rateComments })
    }))
  }

  const getRateCommentsById = (rateCommentsId: string, checkInDate: string):string[] => {
    if (states.rateComments.length == 0) {
      return []
    }

    return  parseAndFilterRateComments(rateCommentsId, states.rateComments, checkInDate)
  }

  const controller:any = {
    fetchRateComments, getRateCommentsById
  }

  return { states, controller }
}