import React, {
  useEffect,
  useState,
  useCallback,
  useMemo,
  Fragment,
  useContext,
} from "react";
import { Row, Col, Drawer, Button } from "antd";
import Head from "next/head";
import Router from "next/router";
import { Element } from "react-scroll";
import qs from "qs";
import config from "../../../customize/config";
import "react-image-lightbox/style.css";
import {
  HotelbedsSearchForm,
  isEmptyObject,
  HotelsAndDestinationsName,
  HotelbedsSourceMarket,
  HotelBedsCheckInDatePicker,
  HotelBedsOccupancyPopover,
  HotelBedsCheckOutDatePicker,
} from "./src/hotelbeds-search-form";
import {
  validateHotelbedsParams,
  TravelCloudClient,
} from "../../../core/travelcloud";
import {
  HotelbedsQuery,
  HotelbedsFormValue,
} from "../../../core/order-product-compute";
import { parseFilters } from "../../../core/component-logic";
import useHotelbedsFilter from "./src/hooks/hotelbeds-filter";
import usePaginateResults from "./src/hooks/paginate-results";
import { useHotelbeds } from "./src/hooks/hotelbeds";
import HotelbedsFilter from "./src/hotelbeds-filter";
import PassengerContent from "./src/hotelbeds-passenger-content";
import CustomizeHotelbedsResult from "./src/hotelbeds-results";
import { NoResult } from "../../pure/NoResult";
import { FilterOutlined, FlagOutlined } from "@ant-design/icons";
import LayoutContext from "../../layouts/LayoutContext";
import HotelbedsDetail from "./src/hotelbeds-details";

export function Hotels(props: any): any {
  const { content } = props;
  const { windowSize, cart } = useContext(LayoutContext);
  // mock client
  const edge2Client: TravelCloudClient = new TravelCloudClient("edge2");
  // initial states
  const [formValue, setFormValue]: [HotelbedsFormValue, any] = useState(
    props.query
  );
  // fetch hotels
  const [result, loading, search, types] = useHotelbeds({
    formValue,
    client: edge2Client,
    cart: cart,
  });
  const [categories, setCategories] = useState([]);
  const [boardCodes, setBoardCodes] = useState([]);
  const [filterDrawer, setfilterDrawer] = useState(false);

  const {
    filterResults,
    setName,
    setBoardCodeParams,
    setCategoryParams,
    setPriceRange,
    filters,
    setFilters,
    minPrice,
    maxPrice,
  } = useHotelbedsFilter();

  const [{ hotel, room, rates }, setSelectedItem]: [any, any] = useState({
    hotel: null,
    room: null,
    rates: null,
  });
  const updateHotel = useCallback(
    (patch: any) => setSelectedItem((prev) => Object.assign(prev, patch)),
    []
  );

  useEffect(() => {
    window.scrollTo(0, 0);
    if (props.query.filters) {
      const parsedParams = parseFilters(props.query.filters) as any;
      setFilters({
        ...filters,
        name: parsedParams.name ? parsedParams.name[0] : "",
        boardCodes: parsedParams.boardCodes ? parsedParams.boardCodes : [],
        categories: parsedParams.categories ? parsedParams.categories : [],
      });
    }
  }, []);

  useEffect(() => {
    if (!isEmptyObject(formValue) && validateHotelbedsParams(formValue)) {
      Router.push("/hotels?" + qs.stringify(formValue));
    }
  }, [formValue]);

  const [flag, setFlag] = useState(false);

  const filteredResults = useMemo(() => {
    return filterResults({
      hotels: result.hotels,
      filters,
      setCategories,
      setBoardCodes,
      Router,
    });
  }, [result.hotels, filters]);

  // const resultsToRender = result.hotels
  const { resultsToRender, loadMore, isLastPage } = usePaginateResults(
    filteredResults,
    10
  );
  const showFilter = (
    <HotelbedsFilter
      filters={filters}
      setName={setName}
      setBoardCodeParams={setBoardCodeParams}
      setCategoryParams={setCategoryParams}
      setPriceRange={setPriceRange}
      boardCodes={boardCodes}
      categories={categories}
      minPrice={minPrice}
      maxPrice={maxPrice}
    />
  );

  return (
    <div id="hotels">
      <Head>
        <title>{config.defaultTitle} | Hotel Vouchers</title>
      </Head>
      <section className="hotels-search wrap">
        <Element name="search">
          <HotelbedsSearchForm formValue={formValue} client={edge2Client}>
            {({ params, controller }) => (
              <React.Fragment>
                <div className="upper-hotel-search">
                  <Row align="middle">
                    <Col>
                      <span className="label">
                        {/* Nationality */}
                        <FlagOutlined />
                      </span>
                    </Col>
                    <Col>
                      <HotelbedsSourceMarket
                        params={params}
                        onSourceMarketChange={controller.onSourceMarketChange}
                      />
                    </Col>
                  </Row>
                </div>
                <div className="lower-hotel-search">
                  <Row gutter={20}>
                    <Col>
                      <div className="custom-auto-complete">
                        <span className="label">Name</span>
                        <HotelsAndDestinationsName
                          className="destination-autocomplete"
                          client={edge2Client}
                          params={params}
                          onHotelsOrDestinationsChange={
                            controller.onHotelsOrDestinationsChange
                          }
                        />
                      </div>
                    </Col>
                    <Col>
                      <div className="custom-date-picker">
                        <span className="label">Check In</span>
                        <HotelBedsCheckInDatePicker
                          params={params}
                          onStayChange={controller.onStayChange}
                        />
                      </div>
                    </Col>
                    <Col>
                      <div className="custom-date-picker">
                        <span className="label">Check Out</span>
                        <HotelBedsCheckOutDatePicker
                          params={params}
                          onStayChange={controller.onStayChange}
                        />
                      </div>
                    </Col>
                    <Col>
                      <HotelBedsOccupancyPopover
                        className="bg-transparent"
                        params={params}
                        onOccupanciesChange={controller.onOccupanciesChange}
                      />
                    </Col>
                    <Col>
                      <Button
                        type="primary"
                        disabled={!validateHotelbedsParams(params)}
                        onClick={(_) => {
                          setFormValue(Object.assign({}, params));
                          if (!isEmptyObject(params)) {
                            search(params);
                            Router.push("/hotels?" + qs.stringify(formValue));
                          }
                        }}
                      >
                        Search
                      </Button>
                    </Col>
                  </Row>
                </div>
              </React.Fragment>
            )}
          </HotelbedsSearchForm>
        </Element>
      </section>
      <section>
        {loading || result.hotels == null || result.hotels.length === 0 ? (
          <NoResult response={result} loading={loading} type="hotels">
            <Element name="result">
              {validateHotelbedsParams(formValue) || ""}
            </Element>
          </NoResult>
        ) : (
          <div className="wrap">
            <Element name="result">
              <section className="result">
                <Row gutter={{ lg: 32 }} justify="space-between">
                  {windowSize < 979 ? (
                    <Fragment>
                      <Button
                        id="filter-mobile"
                        onClick={() => setfilterDrawer(true)}
                      >
                        <FilterOutlined /> FILTER HOTELS
                      </Button>
                      <Drawer
                        placement="top"
                        className="drawer-filter"
                        closable={true}
                        onClose={() => setfilterDrawer(false)}
                        visible={filterDrawer}
                        title="FILTER HOTELS"
                      >
                        {filterDrawer && (
                          <Fragment>
                            {showFilter}
                            <Button onClick={() => setfilterDrawer(false)}>
                              Apply filter
                            </Button>
                          </Fragment>
                        )}
                      </Drawer>
                    </Fragment>
                  ) : (
                    <Col
                      xs={24}
                      sm={24}
                      md={24}
                      lg={6}
                      className="filter-hotel"
                    >
                      <article id="filter-desktop" style={{ padding: 10 }}>
                        {showFilter}
                      </article>
                    </Col>
                  )}
                  <Col xs={24} sm={24} md={24} lg={18}>
                    <CustomizeHotelbedsResult
                      hotels={resultsToRender}
                      types={types}
                      checkIn={result.checkIn}
                      checkOut={result.checkOut}
                      client={edge2Client}
                      onHotelRoomRateClick={(item) => {
                        updateHotel(item);
                        setFlag(true);
                      }}
                    />

                    {isLastPage() ? null : (
                      <div className="center-block more">
                        <Button
                          disabled={isLastPage()}
                          onClick={() => loadMore()}
                        >
                          Load More
                        </Button>
                      </div>
                    )}
                  </Col>
                </Row>
              </section>
            </Element>
          </div>
        )}

        <Drawer
          visible={flag}
          onClose={() => setFlag(false)}
          placement={"right"}
          className="drawer-hotel"
        >
          <div id="hotelbeds-details">
            <HotelbedsDetail
              hotel={hotel}
              room={room}
              rates={rates}
              checkIn={result.checkIn}
              checkOut={result.checkOut}
              client={edge2Client}
              types={types}
            />

            <PassengerContent
              hotel={hotel}
              room={room}
              rates={rates}
              onAddToCart={(bookingForm) => {
                setFlag(false);

                const data = {
                  ...hotel,
                  rooms: [
                    {
                      ...room,
                      rates,
                    },
                  ],
                  checkIn: result.checkIn,
                  checkOut: result.checkOut,
                };

                cart
                  .reset()
                  .addHotelbedsHotel(
                    { static: hotel.details, checkrates: data },
                    bookingForm
                  );
                Router.push("/checkout");
              }}
            />
          </div>
        </Drawer>
      </section>
    </div>
  );
}


export default Hotels;
