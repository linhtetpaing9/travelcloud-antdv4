import React from "react";

const CarouselArrow = ({
  icon,
  className,
  onClick,
}: {
  icon: any;
  className?: any;
  onClick?: any;
}) => {
  return (
    <div className={className} onClick={onClick}>
      {icon}
    </div>
  );
};

export default CarouselArrow