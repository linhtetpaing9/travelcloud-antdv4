import React from "react";
import styles from "../../styles/SingleBanner.module.scss";

const SingleBanner = ({ content }) => {
  return (
    <div className={styles.single_banner}>
      <div
        className={styles.single_banner_bg}
        style={{
          backgroundImage: `url("${content.photo.regular}")`,
        }}
      >
        <div className={styles.single_banner_title}>
          <h1>{content.title}</h1>
        </div>
      </div>
    </div>
  );
};
export default SingleBanner;
